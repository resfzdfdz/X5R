//======================================================================
//  @Filename	:	ctrl.v
//  @Data		:	2021.04.14
//	@Author		:	Li Xiaocong
//	@Department	:	School of microelectronic
//	@Version	:	1.0
//  @Abstract	:	control unit
//	@git		:	https://gitee.com/resfzdfdz/X5R.git
//
//	X5R is a basic 5 stage pipeline implementation of RISC-V ISA;
//	you can redistribute it and/or modify it under the terms of the GNU 
//	General Public License as published by the Free Software Foundation; 
//	either version 3, or (at your option) any later version.
//
//	X5R is distributed in the hope that it will be useful, but WITHOUT ANY
//	WARRANTY; without even the implied warranty of MERCHANTABILITY or
//	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
//	for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with X5R; see the file COPYING3.  If not see
//	<http://www.gnu.org/licenses/>.
//======================================================================
//	This module is pipeline status control unit:
//		1. Stall the pipeline
//		2. Flush the pipeline
//	A signal [4:0] stall is used to represent which register to stall:
//		(stall[0] == 1'b1) means pc stall
//		(stall[1] == 1'b1) means IF/ID stall
//		(stall[2] == 1'b1) means ID/EX stall
//		(stall[3] == 1'b1) means EX/MEM stall
//		(stall[4] == 1'b1) means MEM/WB stall
//	Flush is similar to stall. But for pc, a flush address is needed. 
//	
//	When Stage n stalls, control signals in stage n is muxed to disable.
//======================================================================

module ctrl
#(
	parameter XLEN = 32
)
(
	input wire id_stall_for_lr_hazard,
	input wire bpu_id_flush,
	input wire bpu_ex_flush,
	
	output reg [4:0] stall,
	output reg [4:0] flush
);


always @(*)
begin
	if (id_stall_for_lr_hazard) begin
		stall = 5'b00011;
		flush = 5'b00000;
	end
	else if (bpu_ex_flush) begin
		stall = 5'b00000;
		flush = 5'b00111;
	end
	else if (bpu_id_flush) begin
		stall = 5'b00000;
		flush = 5'b00011;
	end
	else begin
		stall = 5'b00000;
		flush = 5'b00000;
	end
end



endmodule