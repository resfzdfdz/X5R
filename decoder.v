//======================================================================
//  @Filename	:	decoder.v
//  @Data		:	2021.04.11
//	@Author		:	Li Xiaocong
//	@Department	:	School of microelectronic
//	@Version	:	1.0
//  @Abstract	:	Instruction Decoder
//	@git		:	https://gitee.com/resfzdfdz/X5R.git
//
//	X5R is a basic 5 stage pipeline implementation of RISC-V ISA;
//	you can redistribute it and/or modify it under the terms of the GNU 
//	General Public License as published by the Free Software Foundation; 
//	either version 3, or (at your option) any later version.
//
//	X5R is distributed in the hope that it will be useful, but WITHOUT ANY
//	WARRANTY; without even the implied warranty of MERCHANTABILITY or
//	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
//	for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with X5R; see the file COPYING3.  If not see
//	<http://www.gnu.org/licenses/>.
//======================================================================
//	This module is Instruction Decoder. In this stage, below operations
//	will be done:
//		1. immediate number sign extension.
//		2. check hazard between load and other instructions
//		3. derive functional unit(IntUnit and ShiftUnit) control word 
//		4. derive which data will be sent into functional unit
//		5. check if instruction is valid
//		6. derive which data will be written into register file
//		7. derive if this instruction is a jump instrucion, and predict
//		   the direction of branch instructions. I use static branch 
//		   predictor in bpu.
//======================================================================

module decoder
#(
	parameter XLEN = 32
)
(
	input wire [XLEN-1:0]	id_pc,
	input wire [XLEN-1:0] 	id_inst,
	input wire 				id_ex_rd_we,
	input wire [4:0]		id_ex_rd_num,
	input wire 				id_ex_memory_re,
	//	For functional unit
	output reg [2:0] 		int_unit_type_word,
	output reg [1:0] 		int_unit_ctrl_word,
	output reg [1:0] 		shift_unit_ctrl_word,
	//	Immediate number
	output reg [XLEN-1:0] 	id_imm,
	//	Instruction valid
	output reg				id_inst_valid,
	//	Bypass mux select
	output reg 				id_mux_vj,
	output reg 				id_mux_vk,
	output reg 				id_mux_auipc,
	//	Output mux select
	output reg [2:0]		id_mux_output,
	//	For memory control
	output wire [2:0]		id_ls_type,
	output wire 			id_memory_re,
	output wire 			id_memory_we,
	//	For Register File
	output wire [4:0]		id_rd_num,
	output wire 			id_rd_we,
	output wire [4:0]		id_rs1_num,
	output wire				id_rs1_re,
	output wire [4:0]		id_rs2_num,
	output wire				id_rs2_re,
	//	Stall for load and R/RI/S data dependence
	output wire				id_stall_for_lr_hazard,
	//	Branch Predictor
	output reg [1:0]		id_jump_type,
	output wire				id_branch_direction,
	output reg [XLEN-1:0]	id_jump_addr
);

parameter 	OPCODE_R 	= 	7'b0110011,
			OPCODE_RI	=	7'b0010011,
			OPCODE_LD	=	7'b0000011,
			OPCODE_SR	=	7'b0100011,
			OPCODE_LUI	=	7'b0110111,
			OPCODE_AUIPC=	7'b0010111,
			OPCODE_JAL	=	7'b1101111,
			OPCODE_JALR	=	7'b1100111,
			OPCODE_B	=	7'b1100011;
			
parameter	F3R_ADDSUB	=	3'b000,
			F3R_SLL		=	3'b001,
			F3R_SLT		=	3'b010,
			F3R_SLTU	=	3'b011,
			F3R_XOR		=	3'b100,
			F3R_SR		=	3'b101,
			F3R_OR		=	3'b110,
			F3R_AND		=	3'b111;
			
parameter	F3_LB		=	3'b000,
			F3_LH		=	3'b001,
			F3_LW		=	3'b010,
			F3_LBU		=	3'b100,
			F3_LHU		=	3'b101,
			F3_SB		=	3'b000,
			F3_SH		=	3'b001,
			F3_SW		=	3'b010;

parameter	C_ADD		=	2'b10,
			C_SUB		=	2'b11,
			C_SIGN_CP	=	2'b10,
			C_UNSIGN_CP	=	2'b00;	
			
parameter	F3_BEQ		=	3'b000,
			F3_BNE		=	3'b001,
			F3_BLT		=	3'b100,
			F3_BGE		=	3'b101,
			F3_BLTU		=	3'b110,
			F3_BGEU		=	3'b111;

//	Instruction field parser
wire [6:0] opcode;
wire [4:0] rd_num;
wire [2:0] funct3;
wire [4:0] rs1_num;
wire [4:0] rs2_num;
wire [6:0] funct7;

wire [XLEN-1:0] imm_i;
wire [XLEN-1:0] imm_s;
wire [XLEN-1:0] imm_u;
wire [XLEN-1:0]	imm_b;
wire [XLEN-1:0] imm_uj;

reg temp_id_rs1_re;
reg temp_id_rs2_re;
reg temp_id_rd_we;

wire hazard_rs1;
wire hazard_rs2;

assign opcode 	= id_inst[6:0];
assign rd_num 	= id_inst[11:7];
assign funct3 	= id_inst[14:12];
assign rs1_num	= id_inst[19:15];
assign rs2_num	= id_inst[24:20];
assign funct7 	= id_inst[31:25];

assign id_rd_num 	= rd_num;
assign id_rs1_num 	= rs1_num;
assign id_rs2_num	= rs2_num;

//	More type of immediate number type can be added here.
assign imm_i  = { {(20){id_inst[31]}}, id_inst[31:20] };
assign imm_s  = { {(20){id_inst[31]}}, id_inst[31:25], id_inst[11:7] };
assign imm_u  = { id_inst[31:12], 12'h0};
assign imm_b  = { {(19){id_inst[31]}}, id_inst[31], id_inst[7], id_inst[30:25], id_inst[11:8], 1'b0 };
assign imm_uj = { {(11){id_inst[31]}}, id_inst[31], id_inst[20],id_inst[19:12], id_inst[30:21], 1'b0 };

//	Data dependent detection
assign hazard_rs1 = id_ex_memory_re & id_ex_rd_we && temp_id_rs1_re && (id_ex_rd_num == rs1_num);
assign hazard_rs2 = id_ex_memory_re & id_ex_rd_we && temp_id_rs2_re && (id_ex_rd_num == rs2_num);
assign id_stall_for_lr_hazard = (hazard_rs1 | hazard_rs2) ? 1'b1 : 1'b0;

//	JALR stall consition
//	assign id_stall_for_jalr = (id_ex_jump_type != 2'b11) && (opcode == OPCODE_JALR);

//	Flush control signals when pipeline stall in ID stage
assign id_rs1_re = (id_stall_for_lr_hazard ) ? 1'b0 : temp_id_rs1_re;
						
assign id_rs2_re = (id_stall_for_lr_hazard ) ? 1'b0 : temp_id_rs2_re;
						
assign id_rd_we  = (id_stall_for_lr_hazard ) ? 1'b0 : temp_id_rd_we;


always @(*)
begin
	case (opcode)
		OPCODE_LD,
		OPCODE_RI,
		OPCODE_JALR:	id_imm = imm_i;		// output i type for LX, RI, JALR
		OPCODE_SR:		id_imm = imm_s; 	// output s type for SX
		OPCODE_LUI,
		OPCODE_AUIPC:	id_imm = imm_u;		// output u type for LUI, AUIPC
		OPCODE_B:		id_imm = imm_b;		// output b type for BXX
		OPCODE_JAL:		id_imm = imm_uj;	// output uj type for JAL
		default:		id_imm = 32'h0;
	endcase
end

//	load/store type 
assign id_ls_type = funct3;
assign id_memory_re = id_stall_for_lr_hazard ? 1'b0 : (opcode == OPCODE_LD);
assign id_memory_we = id_stall_for_lr_hazard ? 1'b0 : (opcode == OPCODE_SR);

//********	Instruction decode	********

//	Part 1: Functional unit control word decode
always @(*)
begin
	case (opcode)
		OPCODE_R:	
			case (funct3)
				F3R_ADDSUB:
					begin
						if (funct7[5]) begin
							int_unit_type_word 		= 3'b000;
							int_unit_ctrl_word 		= C_SUB;
							shift_unit_ctrl_word 	= 2'b01;
						end
						else begin
							int_unit_type_word 		= 3'b000;
							int_unit_ctrl_word 		= C_ADD;
							shift_unit_ctrl_word 	= 2'b01;
						end
					end
				F3R_SLL:
					begin
							int_unit_type_word 		= 3'b000;
							int_unit_ctrl_word 		= C_ADD;
							shift_unit_ctrl_word 	= 2'b00;
					end
				F3R_SLT:
					begin
							int_unit_type_word 		= 3'b001;
							int_unit_ctrl_word 		= C_SIGN_CP;
							shift_unit_ctrl_word 	= 2'b01;
					end
				F3R_SLTU:
					begin
							int_unit_type_word 		= 3'b001;
							int_unit_ctrl_word 		= C_UNSIGN_CP;
							shift_unit_ctrl_word 	= 2'b01;
					end
				F3R_XOR:
					begin
							int_unit_type_word 		= 3'b101;
							int_unit_ctrl_word 		= C_ADD;
							shift_unit_ctrl_word 	= 2'b01;
					end	
				F3R_SR:
					begin
						if (funct7[5]) begin
							int_unit_type_word 		= 3'b000;
							int_unit_ctrl_word 		= C_ADD;
							shift_unit_ctrl_word 	= 2'b11;
						end
						else begin
							int_unit_type_word 		= 3'b000;
							int_unit_ctrl_word 		= C_ADD;
							shift_unit_ctrl_word 	= 2'b10;
						end
					end
				F3R_OR:
					begin
							int_unit_type_word 		= 3'b111;
							int_unit_ctrl_word 		= C_ADD;
							shift_unit_ctrl_word 	= 2'b11;
					end
				F3R_AND:
					begin
							int_unit_type_word 		= 3'b110;
							int_unit_ctrl_word 		= C_ADD;
							shift_unit_ctrl_word 	= 2'b11;
					end
				default:
					begin
							int_unit_type_word 		= 3'b000;
							int_unit_ctrl_word 		= C_ADD;
							shift_unit_ctrl_word 	= 2'b01;
					end
			endcase
			
			
			
		OPCODE_RI:
			case (funct3)
				F3R_ADDSUB:
					begin
							int_unit_type_word 		= 3'b000;
							int_unit_ctrl_word 		= C_ADD;
							shift_unit_ctrl_word 	= 2'b01;
					end
				F3R_SLL:
					begin
							int_unit_type_word 		= 3'b000;
							int_unit_ctrl_word 		= C_ADD;
							shift_unit_ctrl_word 	= 2'b00;
					end
				F3R_SLT:
					begin
							int_unit_type_word 		= 3'b001;
							int_unit_ctrl_word 		= C_SIGN_CP;
							shift_unit_ctrl_word 	= 2'b01;
					end
				F3R_SLTU:
					begin
							int_unit_type_word 		= 3'b001;
							int_unit_ctrl_word 		= C_UNSIGN_CP;
							shift_unit_ctrl_word 	= 2'b01;
					end
				F3R_XOR:
					begin
							int_unit_type_word 		= 3'b101;
							int_unit_ctrl_word 		= C_ADD;
							shift_unit_ctrl_word 	= 2'b01;
					end	
				F3R_SR:
					begin
						if (funct7[5]) begin
							int_unit_type_word 		= 3'b000;
							int_unit_ctrl_word 		= C_ADD;
							shift_unit_ctrl_word 	= 2'b11;
						end
						else begin
							int_unit_type_word 		= 3'b000;
							int_unit_ctrl_word 		= C_ADD;
							shift_unit_ctrl_word 	= 2'b10;
						end
					end
				F3R_OR:
					begin
							int_unit_type_word 		= 3'b111;
							int_unit_ctrl_word 		= C_ADD;
							shift_unit_ctrl_word 	= 2'b11;
					end
				F3R_AND:
					begin
							int_unit_type_word 		= 3'b110;
							int_unit_ctrl_word 		= C_ADD;
							shift_unit_ctrl_word 	= 2'b11;
					end
				default:
					begin
							int_unit_type_word 		= 3'b000;
							int_unit_ctrl_word 		= C_ADD;
							shift_unit_ctrl_word 	= 2'b01;
					end
			endcase	

			
			
		OPCODE_LD,
		OPCODE_SR:
			begin
				int_unit_type_word 		= 3'b000;
				int_unit_ctrl_word 		= C_ADD;
				shift_unit_ctrl_word 	= 2'b01;
			end


		OPCODE_LUI:
			begin
				int_unit_type_word 		= 3'b000;
				int_unit_ctrl_word 		= C_ADD;
				shift_unit_ctrl_word 	= 2'b01;
			end

		OPCODE_AUIPC:
			begin
				int_unit_type_word 		= 3'b000;
				int_unit_ctrl_word 		= C_ADD;
				shift_unit_ctrl_word 	= 2'b01;
			end
			
		OPCODE_B:
			case (funct3)
				F3_BEQ:
					begin
						int_unit_type_word 		= 3'b011;
						int_unit_ctrl_word 		= C_SIGN_CP;
						shift_unit_ctrl_word 	= 2'b01;
					end
				F3_BNE:
					begin
						int_unit_type_word 		= 3'b100;
						int_unit_ctrl_word 		= C_SIGN_CP;
						shift_unit_ctrl_word 	= 2'b01;
					end
				F3_BLT:
					begin
						int_unit_type_word 		= 3'b001;
						int_unit_ctrl_word 		= C_SIGN_CP;
						shift_unit_ctrl_word 	= 2'b01;
					end
				F3_BGE:
					begin
						int_unit_type_word 		= 3'b010;
						int_unit_ctrl_word 		= C_SIGN_CP;
						shift_unit_ctrl_word 	= 2'b01;
					end
				F3_BLTU:
					begin
						int_unit_type_word 		= 3'b001;
						int_unit_ctrl_word 		= C_UNSIGN_CP;
						shift_unit_ctrl_word 	= 2'b01;
					end
				F3_BGEU:
					begin
						int_unit_type_word 		= 3'b010;
						int_unit_ctrl_word 		= C_UNSIGN_CP;
						shift_unit_ctrl_word 	= 2'b01;
					end
				default:
					begin
						int_unit_type_word 		= 3'b000;
						int_unit_ctrl_word 		= C_ADD;
						shift_unit_ctrl_word 	= 2'b01;
					end
			endcase
			
		OPCODE_JAL:
			begin
				int_unit_type_word 		= 3'b000;
				int_unit_ctrl_word 		= C_ADD;
				shift_unit_ctrl_word 	= 2'b01;
			end
		
		OPCODE_JALR:
			begin
				int_unit_type_word 		= 3'b000;
				int_unit_ctrl_word 		= C_ADD;
				shift_unit_ctrl_word 	= 2'b01;
			end
			
		default:
			begin
				int_unit_type_word 		= 3'b000;
				int_unit_ctrl_word 		= C_ADD;
				shift_unit_ctrl_word 	= 2'b01;
			end
	endcase
end

//	Part 2: Bypass mux select decode
always @(*)
begin
	case (opcode)
		OPCODE_R:	
			begin
				id_mux_vj 		= 1'b1;		//	Vj mux select rs1.
				id_mux_vk 		= 1'b1;		//	Vk mux select rs2.
				id_mux_auipc	= 1'b0;		//	Vj mux don't select pc.
			end
		OPCODE_RI,
		OPCODE_LD,
		OPCODE_SR:
			begin
				id_mux_vj 		= 1'b1;		//	Vj mux select rs1.
				id_mux_vk 		= 1'b0;		//	Vk mux select imm.
				id_mux_auipc	= 1'b0;		//	Vj mux don't select pc.
			end
		OPCODE_LUI:
			begin
				id_mux_vj 		= 1'b0;		//	Vj mux select 0.
				id_mux_vk 		= 1'b0;		//	Vk mux select imm.
				id_mux_auipc	= 1'b0;		//	Vj mux don't select pc.
			end
		OPCODE_AUIPC:
			begin
				id_mux_vj 		= 1'b0;		//	Vj mux select 0.
				id_mux_vk 		= 1'b0;		//	Vk mux select imm.
				id_mux_auipc	= 1'b1;		//	Vj mux select pc.
			end
		OPCODE_B:
			begin
				id_mux_vj 		= 1'b1;		//	Vj mux select rs1.
				id_mux_vk 		= 1'b1;		//	Vk mux select rs2.
				id_mux_auipc	= 1'b0;		//	Vj mux don't select pc.
			end
		OPCODE_JAL:
			begin
				id_mux_vj 		= 1'b0;		//	Vj mux select 0.
				id_mux_vk 		= 1'b0;		//	Vk mux select imm.
				id_mux_auipc	= 1'b0;		//	Vj mux don't select pc.
			end
		OPCODE_JALR:
			begin
				id_mux_vj 		= 1'b1;		//	Vj mux select rs1.
				id_mux_vk 		= 1'b0;		//	Vk mux select imm.
				id_mux_auipc	= 1'b0;		//	Vj mux don't select pc.
			end

		default:
			begin
				id_mux_vj 		= 1'b0;		//	Vj mux select 0.
				id_mux_vk 		= 1'b0;		//	Vk mux select imm.
				id_mux_auipc	= 1'b0;		//	Vj mux don't select pc.
			end
	endcase
end

//	Part 3: Instruction valid decode
always @(*)
begin
	case (opcode)
		OPCODE_R, 
		OPCODE_RI,
		OPCODE_LD,
		OPCODE_SR,
		OPCODE_LUI,
		OPCODE_AUIPC,
		OPCODE_JAL,
		OPCODE_JALR,
		OPCODE_B:		id_inst_valid	=	1'b1;
		default:		id_inst_valid	=	1'b0;
	endcase
end

//	Part 4:	Register read and write control
always @(*)
begin
	case (opcode)
		OPCODE_R:
			begin
				temp_id_rd_we	=	1'b1;
				temp_id_rs1_re	=	1'b1;
				temp_id_rs2_re	=	1'b1;
			end
		OPCODE_RI:
			begin
				temp_id_rd_we	=	1'b1;
				temp_id_rs1_re	=	1'b1;
				temp_id_rs2_re	=	1'b0;
			end	
		OPCODE_LD:
			begin
				temp_id_rd_we	=	1'b1;
				temp_id_rs1_re	=	1'b1;
				temp_id_rs2_re	=	1'b0;
			end	
		OPCODE_SR:
			begin
				temp_id_rd_we	=	1'b0;
				temp_id_rs1_re	=	1'b1;
				temp_id_rs2_re	=	1'b1;
			end
		OPCODE_LUI,
		OPCODE_AUIPC:
			begin
				temp_id_rd_we	=	1'b1;
				temp_id_rs1_re	=	1'b0;
				temp_id_rs2_re	=	1'b0;
			end
		OPCODE_B:
			begin
				temp_id_rd_we	=	1'b0;
				temp_id_rs1_re	=	1'b1;
				temp_id_rs2_re	=	1'b1;
			end
		OPCODE_JAL:
			begin
				temp_id_rd_we	=	1'b1;
				temp_id_rs1_re	=	1'b0;
				temp_id_rs2_re	=	1'b0;
			end	
		OPCODE_JALR:
			begin
				temp_id_rd_we	=	1'b1;
				temp_id_rs1_re	=	1'b1;
				temp_id_rs2_re	=	1'b0;
			end	
			
		default:
			begin
				temp_id_rd_we	=	1'b0;
				temp_id_rs1_re	=	1'b0;
				temp_id_rs2_re	=	1'b0;
			end	
	endcase
end

//	Part 5:	Output mux select decode
always @(*)
begin
	case (opcode)
		OPCODE_R,
		OPCODE_RI:
			case (funct3)
				F3R_SLL,
				F3R_SR:		id_mux_output	=	3'b001;	//output from shift unit
				default:	id_mux_output	=	3'b010; //output from int unit
			endcase
		OPCODE_LD,
		OPCODE_SR:			id_mux_output	=	3'b010; //output from int unit
		OPCODE_AUIPC:		id_mux_output	=	3'b010; //output from int unit
		OPCODE_LUI:			id_mux_output	=	3'b011; //outptu from u type immediate number
		OPCODE_B,
		OPCODE_JAL:			id_mux_output	=	3'b000;	//output 0
		OPCODE_JALR:		id_mux_output	=	3'b100;	//output pc + 4
		default:			id_mux_output	=	3'b000; //output 0
	endcase
end

//	Part 6: Branch predictor decode
always @(*)
begin
	if (id_stall_for_lr_hazard)
		id_jump_type	=	2'b00;
	else
		case (opcode)
			OPCODE_JAL:			id_jump_type	=	2'b10;
			OPCODE_JALR:		id_jump_type	=	2'b11;
			OPCODE_B:			id_jump_type	=	2'b01;
			default:			id_jump_type	=	2'b00;
		endcase
end

// (id_branch_direction == 1) means (imm_b < 0), 
//	this is a branch from back to front,
//	thus it is predicted to "branch taken"
assign id_branch_direction 	= 	id_inst[31];

always @(*)
begin
	case (opcode)
		OPCODE_B:		id_jump_addr = id_pc + imm_b;
		OPCODE_JAL:		id_jump_addr = id_pc + imm_uj;
		OPCODE_JALR:	id_jump_addr = 32'h0;	//	don't care because jalr address is from EX stage
		default:		id_jump_addr = 32'h0;
	endcase
end

//********	Instruction decode end	********



endmodule