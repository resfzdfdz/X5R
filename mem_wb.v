//======================================================================
//  @Filename	:	mem_wb.v
//  @Data		:	2021.04.11
//	@Author		:	Li Xiaocong
//	@Department	:	School of microelectronic
//	@Version	:	1.0
//  @Abstract	:	MEM/WB Pipeline wireister
//	@git		:	https://gitee.com/resfzdfdz/X5R.git
//
//	X5R is a basic 5 stage pipeline implementation of RISC-V ISA;
//	you can redistribute it and/or modify it under the terms of the GNU 
//	General Public License as published by the Free Software Foundation; 
//	either version 3, or (at your option) any later version.
//
//	X5R is distributed in the hope that it will be useful, but WITHOUT ANY
//	WARRANTY; without even the implied warranty of MERCHANTABILITY or
//	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
//	for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with X5R; see the file COPYING3.  If not see
//	<http://www.gnu.org/licenses/>.
//======================================================================
//	This module is EX/MEM Pipeline register
//======================================================================

module mem_wb
#(
	parameter XLEN = 32
)
(
	input wire 				clk,
	input wire 				rst,
	input wire				mem_wb_stall,
	input wire				mem_wb_flush,
	//	Input from EX/MEM
	input wire [4:0]		ex_mem_rd_num,
	input wire 				ex_mem_rd_we,	
	input wire [4:0]		ex_mem_rs1_num,
	input wire				ex_mem_rs1_re,
	input wire [4:0]		ex_mem_rs2_num,
	input wire				ex_mem_rs2_re,
	input wire [XLEN-1:0]	ex_mem_rd_value,
	input wire 				ex_mem_memory_re,
	//	Input from data memory
	input wire [XLEN-1:0]	mem_memory_dout,
	input wire 				mem_addr_align,
	
	output reg [4:0]		mem_wb_rd_num,
	output reg 				mem_wb_rd_we,
	output reg [XLEN-1:0]	mem_wb_rd_value,
	output reg				mem_wb_memory_re,
	
	output reg [XLEN-1:0]	mem_wb_memory_dout,
	output reg				mem_wb_addr_align,
	
	output reg [4:0]		mem_wb_rs1_num,
	output reg				mem_wb_rs1_re,
	output reg [4:0]		mem_wb_rs2_num,
	output reg				mem_wb_rs2_re

);


always @(posedge clk)
begin
	if (rst) begin
		mem_wb_rd_num		<=	0;
		mem_wb_rd_we		<=	0;
        mem_wb_rd_value		<=	0;	
		mem_wb_memory_dout	<=	0;
		mem_wb_addr_align	<=	0;
		mem_wb_memory_re	<=	0;
		mem_wb_rs1_num		<=	0;
		mem_wb_rs1_re		<=	0;
		mem_wb_rs2_num		<=	0;
		mem_wb_rs2_re		<=	0;
	end
	else if (mem_wb_flush) begin
		mem_wb_rd_num		<=	0;
		mem_wb_rd_we		<=	0;
        mem_wb_rd_value		<=	0;	
		mem_wb_memory_dout	<=	0;
		mem_wb_addr_align	<=	0;
		mem_wb_memory_re	<=	0;
		mem_wb_rs1_num		<=	0;
		mem_wb_rs1_re		<=	0;
		mem_wb_rs2_num		<=	0;
		mem_wb_rs2_re		<=	0;
	end
	else if (mem_wb_stall) begin
		mem_wb_rd_num		<=	mem_wb_rd_num		;
		mem_wb_rd_we		<=	mem_wb_rd_we		;
        mem_wb_rd_value		<=	mem_wb_rd_value		;
		mem_wb_memory_dout	<=	mem_wb_memory_dout	;
		mem_wb_addr_align	<=	mem_wb_addr_align	;
		mem_wb_memory_re	<=	mem_wb_memory_re	;	
		mem_wb_rs1_num		<=	mem_wb_rs1_num		;
		mem_wb_rs1_re		<=	mem_wb_rs1_re		;
		mem_wb_rs2_num		<=	mem_wb_rs2_num		;
		mem_wb_rs2_re		<=	mem_wb_rs2_re		;
	end
	begin
		mem_wb_rd_num		<=	ex_mem_rd_num		;
		mem_wb_rd_we		<=	ex_mem_rd_we		;
        mem_wb_rd_value		<=	ex_mem_rd_value		;
		mem_wb_memory_dout	<=	mem_memory_dout		;
		mem_wb_addr_align	<=	mem_addr_align		;
		mem_wb_memory_re	<=	ex_mem_memory_re	;
		mem_wb_rs1_num		<=	ex_mem_rs1_num		;
		mem_wb_rs1_re		<=	ex_mem_rs1_re		;
		mem_wb_rs2_num		<=	ex_mem_rs2_num		;
		mem_wb_rs2_re		<=	ex_mem_rs2_re		;
	end
end


endmodule