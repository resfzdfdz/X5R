//======================================================================
//  @Filename	:	data_mem_model.v
//  @Data		:	2021.04.11
//	@Author		:	Li Xiaocong
//	@Department	:	School of microelectronic
//	@Version	:	1.0
//  @Abstract	:	Data Memory Model
//	@git		:	https://gitee.com/resfzdfdz/X5R.git
//
//	X5R is a basic 5 stage pipeline implementation of RISC-V ISA;
//	you can redistribute it and/or modify it under the terms of the GNU 
//	General Public License as published by the Free Software Foundation; 
//	either version 3, or (at your option) any later version.
//
//	X5R is distributed in the hope that it will be useful, but WITHOUT ANY
//	WARRANTY; without even the implied warranty of MERCHANTABILITY or
//	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
//	for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with X5R; see the file COPYING3.  If not see
//	<http://www.gnu.org/licenses/>.
//======================================================================
//	This module is data memory model, load/store type:
//		LB:		3'b000
//		LH:		3'b001
//		LW:		3'b010
//		LBU:	3'b100
//		LHU:	3'b101
//		
//		SB:		3'b000
//		SH:		3'b001
//		SW:		3'b010
//
//	This is a memory model. In fact, this is a register file.
//	Data memory is little endian mode.
//
//	*********************************************************
//	Modified by Li Xiaocong in 2021.04.30:
//		Use unified memory model instead of seperate inst_mem
//		and data_mem.
//	*********************************************************
//======================================================================

module data_mem_model
#(
	parameter XLEN = 32
)
(
	input wire 				clk,
	input wire 				rst,
	input wire [2:0] 		ls_type,
	input wire 				re,
	input wire 				we,
	input wire [XLEN-1:0]	addr,
	input wire [XLEN-1:0]	din,
	output reg [XLEN-1:0]	dout,
	output reg 				addr_align
);

parameter	F3_LB		=	3'b000,
			F3_LH		=	3'b001,
			F3_LW		=	3'b010,
			F3_LBU		=	3'b100,
			F3_LHU		=	3'b101,
			F3_SB		=	3'b000,
			F3_SH		=	3'b001,
			F3_SW		=	3'b010;

//	Address Align detection
always @(*)
begin
	if (re | we)
		case (ls_type)
			//	If load/store byte, all address is aligned.
			F3_LB,
			F3_LBU,
			F3_SB:		addr_align = 1'b1;	
			//	If load/store half word, (addr[0] == 0) is aligned.
			F3_LH,
			F3_LHU,
			F3_SH:		addr_align = (addr[0] == 1'b0) ? 1'b1 : 1'b0;	
			//	If load/store word, (addr[1:0] == 2'b00) is aligned.
			F3_LW,
			F3_SW:		addr_align = (addr[1:0] == 2'b00) ? 1'b1 : 1'b0;
			default:	addr_align = 1'b1;
		endcase
	else
		addr_align = 1'b1;
end

//	Memory models
reg [7:0] memory [511:0];

wire [XLEN-1:0] addr0, addr1, addr2, addr3;
wire [7:0] byte0, byte1, byte2, byte3;
wire [7:0] b, h;

//	Get all addresses
assign addr0 = {addr[XLEN-1:2], 2'b00};
assign addr1 = {addr[XLEN-1:2], 2'b01};
assign addr2 = {addr[XLEN-1:2], 2'b10};
assign addr3 = {addr[XLEN-1:2], 2'b11};

assign byte0 = memory[addr0];
assign byte1 = memory[addr1];
assign byte2 = memory[addr2];
assign byte3 = memory[addr3];

assign b = memory[addr];
assign h = memory[addr+8];

//	Memory read model (Little-endian mode)
always @(*)
begin
	if (re)
		case (ls_type)
			F3_LB:		dout = { {(24){b[7]}}, b };
			F3_LBU:		dout = { {(24){1'b0}}, b };
			F3_LH:		dout = { {(16){h[7]}}, h, b };
			F3_LHU:		dout = { {(16){1'b0}}, h, b };
			F3_LW:		dout = { byte3, byte2, byte1, byte0 };
			default:	dout = 32'h0;
		endcase
	else
		dout = 32'h0;
end

//	Memory write model (Little-endian mode)
//	Branches not implemented are remaining memory value.
integer i;

always @(posedge clk)
begin
	if (rst)
		for (i = 0; i < 512; i = i+ 1)
			memory[i] <= 32'h0;
	else if (we)
		case (ls_type)
			F3_SB:		begin
							memory[addr] 	<= din[7:0];
						end
			F3_SH:		begin
							memory[addr] 	<= din[7:0];
							memory[addr+8]	<= din[15:8];
						end
			F3_SW:		begin
							memory[addr0]	<= din[7:0];
							memory[addr1]	<= din[15:8];
							memory[addr2]	<= din[23:16];
							memory[addr3]	<= din[31:24];
						end
		endcase
end


endmodule