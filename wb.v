//======================================================================
//  @Filename	:	wb.v
//  @Data		:	2021.04.11
//	@Author		:	Li Xiaocong
//	@Department	:	School of microelectronic
//	@Version	:	1.0
//  @Abstract	:	write back stage
//	@git		:	https://gitee.com/resfzdfdz/X5R.git
//
//	X5R is a basic 5 stage pipeline implementation of RISC-V ISA;
//	you can redistribute it and/or modify it under the terms of the GNU 
//	General Public License as published by the Free Software Foundation; 
//	either version 3, or (at your option) any later version.
//
//	X5R is distributed in the hope that it will be useful, but WITHOUT ANY
//	WARRANTY; without even the implied warranty of MERCHANTABILITY or
//	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
//	for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with X5R; see the file COPYING3.  If not see
//	<http://www.gnu.org/licenses/>.
//======================================================================
//	This module is write back stage
//======================================================================

module wb
#(
	parameter XLEN = 32
)
(
	input wire [4:0]		mem_wb_rd_num,
	input wire 				mem_wb_rd_we,
	input wire [XLEN-1:0]	mem_wb_rd_value,
	input wire				mem_wb_memory_re,
	input wire [XLEN-1:0]	mem_wb_memory_dout,
	input wire				mem_wb_addr_align,
	
	output wire [4:0]		wb_rd_num,
	output wire 			wb_rd_we,
	output wire [XLEN-1:0]	wb_rd_value
);

//	Select data from functional unit or memory
assign wb_rd_num 	= mem_wb_rd_num;
assign wb_rd_we 	= mem_wb_rd_we;
assign wb_rd_value 	= mem_wb_memory_re ? mem_wb_memory_dout : mem_wb_rd_value;


endmodule