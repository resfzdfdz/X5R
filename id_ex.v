//======================================================================
//  @Filename	:	id_ex.v
//  @Data		:	2021.04.11
//	@Author		:	Li Xiaocong
//	@Department	:	School of microelectronic
//	@Version	:	1.0
//  @Abstract	:	ID/EX Pipeline Register
//	@git		:	https://gitee.com/resfzdfdz/X5R.git
//
//	X5R is a basic 5 stage pipeline implementation of RISC-V ISA;
//	you can redistribute it and/or modify it under the terms of the GNU 
//	General Public License as published by the Free Software Foundation; 
//	either version 3, or (at your option) any later version.
//
//	X5R is distributed in the hope that it will be useful, but WITHOUT ANY
//	WARRANTY; without even the implied warranty of MERCHANTABILITY or
//	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
//	for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with X5R; see the file COPYING3.  If not see
//	<http://www.gnu.org/licenses/>.
//======================================================================
//	This module is ID/EX Pipeline Register
//======================================================================

module id_ex
#(
	parameter XLEN = 32
)
(
	input wire 				clk,
	input wire 				rst,
	input wire 				id_ex_stall,
	input wire 				id_ex_flush,
	//	From IF/ID register
	input wire [XLEN-1:0]	id_pc,
	input wire 				id_align,
	//	For functional unit
	input wire [2:0] 		int_unit_type_word,
	input wire [1:0] 		int_unit_ctrl_word,
	input wire [1:0] 		shift_unit_ctrl_word,
	//	Immediate number
	input wire [XLEN-1:0] 	id_imm,
	//	Instruction valid
	input wire				id_inst_valid,
	//	Bypass mux select
	input wire 				id_mux_vj,
	input wire 				id_mux_vk,
	input reg 				id_mux_auipc,
	//	Output mux select
	input wire [2:0] 		id_mux_output,
	//	For memory control
	input wire [2:0]		id_ls_type,
	input wire 				id_memory_re,
	input wire 				id_memory_we,
	//	For Register File
	input wire [4:0]		id_rd_num,
	input wire 				id_rd_we,
	input wire [4:0]		id_rs1_num,
	input wire				id_rs1_re,
	input wire [4:0]		id_rs2_num,
	input wire				id_rs2_re,
	//	Register File Output
	input wire [XLEN-1:0]	id_rs1_value,
	input wire [XLEN-1:0]	id_rs2_value,
	//	Stall for load and R/RI/S data dependence
	input wire 				id_stall_for_lr_hazard,
	//	Branch Predictor
	input wire [1:0]		id_jump_type,
	input wire				id_branch_direction,
	input wire [XLEN-1:0]	id_jump_addr,
	
	
	output reg [XLEN-1:0]	id_ex_pc,
	output reg 				id_ex_align,
	//	For functional unit
	output reg [2:0] 		id_ex_int_unit_type_word,
	output reg [1:0] 		id_ex_int_unit_ctrl_word,
	output reg [1:0] 		id_ex_shift_unit_ctrl_word,
	//	Immediate number
	output reg [XLEN-1:0] 	id_ex_imm,
	//	Instruction valid
	output reg				id_ex_inst_valid,
	//	Bypass mux select
	output reg 				id_ex_mux_vj,
	output reg 				id_ex_mux_vk,
	output reg 				id_ex_mux_auipc,
	//	Output mux select
	output reg [2:0]		id_ex_mux_output,
	//	For memory control
	output reg [2:0]		id_ex_ls_type,
	output reg 				id_ex_memory_re,
	output reg 				id_ex_memory_we,
	//	For Register File
	output reg [4:0]		id_ex_rd_num,
	output reg 				id_ex_rd_we,
	output reg [4:0]		id_ex_rs1_num,
	output reg				id_ex_rs1_re,
	output reg [4:0]		id_ex_rs2_num,
	output reg				id_ex_rs2_re,
	//	Register File Output
	output reg [XLEN-1:0]	id_ex_rs1_value,
	output reg [XLEN-1:0]	id_ex_rs2_value,
	//	Branch Predictor
	output reg [1:0]		id_ex_jump_type,
	output reg				id_ex_branch_direction,
	output reg [XLEN-1:0]	id_ex_jump_addr	
);

always @(posedge clk)
begin
	if (rst) begin
		id_ex_pc						<=	0;
		id_ex_align						<=	0;
		id_ex_int_unit_type_word		<=	0;
		id_ex_int_unit_ctrl_word		<=	0;
		id_ex_shift_unit_ctrl_word		<=	0;
		id_ex_imm						<=	0;
		id_ex_inst_valid				<=	0;
		id_ex_mux_vj					<=	0;
		id_ex_mux_vk					<=	0;
		id_ex_mux_output				<=	0;
		id_ex_rd_num					<=	0;
		id_ex_rd_we						<=	0;
		id_ex_rs1_num					<=	0;
		id_ex_rs1_re					<=	0;
		id_ex_rs2_num					<=	0;
		id_ex_rs2_re					<=	0;
		id_ex_rs1_value					<=	0;
		id_ex_rs2_value					<=	0;
		id_ex_ls_type					<=	0;
		id_ex_memory_re					<=	0;
		id_ex_memory_we					<=	0;
		id_ex_jump_type					<=	0;
		id_ex_branch_direction			<=	0;
		id_ex_jump_addr					<=	0;
	end
	else if (id_ex_flush) begin
		id_ex_pc						<=	0;
		id_ex_align						<=	0;
		id_ex_int_unit_type_word		<=	0;
		id_ex_int_unit_ctrl_word		<=	0;
		id_ex_shift_unit_ctrl_word		<=	0;
		id_ex_imm						<=	0;
		id_ex_inst_valid				<=	0;
		id_ex_mux_vj					<=	0;
		id_ex_mux_vk					<=	0;
		id_ex_mux_output				<=	0;
		id_ex_rd_num					<=	0;
		id_ex_rd_we						<=	0;
		id_ex_rs1_num					<=	0;
		id_ex_rs1_re					<=	0;
		id_ex_rs2_num					<=	0;
		id_ex_rs2_re					<=	0;
		id_ex_rs1_value					<=	0;
		id_ex_rs2_value					<=	0;
		id_ex_ls_type					<=	0;
		id_ex_memory_re					<=	0;
		id_ex_memory_we					<=	0;
		id_ex_jump_type					<=	0;
		id_ex_branch_direction			<=	0;
		id_ex_jump_addr					<=	0;
	end
	else if (id_ex_stall) begin
		id_ex_pc						<=	id_ex_pc					;
		id_ex_align						<=	id_ex_align					;
		id_ex_int_unit_type_word		<=	id_ex_int_unit_type_word	;
		id_ex_int_unit_ctrl_word		<=	id_ex_int_unit_ctrl_word	;
		id_ex_shift_unit_ctrl_word		<=	id_ex_shift_unit_ctrl_word	;
		id_ex_imm						<=	id_ex_imm					;
		id_ex_inst_valid				<=	id_ex_inst_valid			;
		id_ex_mux_vj					<=	id_ex_mux_vj				;
		id_ex_mux_vk					<=	id_ex_mux_vk				;
		id_ex_mux_auipc					<=	id_ex_mux_auipc				;
		id_ex_mux_output				<=	id_ex_mux_output			;
		id_ex_rd_num					<=	id_ex_rd_num				;
		id_ex_rd_we						<=	id_ex_rd_we					;
		id_ex_rs1_num					<=	id_ex_rs1_num				;
		id_ex_rs1_re					<=	id_ex_rs1_re				;
		id_ex_rs2_num					<=	id_ex_rs2_num				;
		id_ex_rs2_re					<=	id_ex_rs2_re				;
		id_ex_rs1_value					<=	id_ex_rs1_value				;
		id_ex_rs2_value					<=	id_ex_rs2_value				;
		id_ex_ls_type					<=	id_ex_ls_type				;
		id_ex_memory_re					<=	id_ex_memory_re				;
		id_ex_memory_we					<=	id_ex_memory_we				;	
		id_ex_jump_type					<=	id_ex_jump_type				;
		id_ex_branch_direction			<=	id_ex_branch_direction		;
		id_ex_jump_addr					<=	id_ex_jump_addr				;
	end
	else begin
		id_ex_pc						<=	id_pc					;
		id_ex_align						<=	id_align				;
		id_ex_int_unit_type_word		<=	int_unit_type_word		;
		id_ex_int_unit_ctrl_word		<=	int_unit_ctrl_word		;
		id_ex_shift_unit_ctrl_word		<=	shift_unit_ctrl_word	;
		id_ex_imm						<=	id_imm					;
		id_ex_inst_valid				<=	id_inst_valid			;
		id_ex_mux_vj					<=	id_mux_vj				;
		id_ex_mux_vk					<=	id_mux_vk				;
		id_ex_mux_auipc					<=	id_mux_auipc			;
		id_ex_mux_output				<=	id_mux_output			;
		id_ex_rd_num					<=	id_rd_num				;
		id_ex_rd_we						<=	id_rd_we				;
		id_ex_rs1_num					<=	id_rs1_num				;
		id_ex_rs1_re					<=	id_rs1_re				;
		id_ex_rs2_num					<=	id_rs2_num				;
		id_ex_rs2_re					<=	id_rs2_re				;
		id_ex_rs1_value					<=	id_rs1_value			;
		id_ex_rs2_value					<=	id_rs2_value			;
		id_ex_ls_type					<=	id_ls_type				;
		id_ex_memory_re					<=	id_memory_re			;
		id_ex_memory_we					<=	id_memory_we			;
		id_ex_jump_type					<=	id_jump_type			;		
		id_ex_branch_direction			<=	id_branch_direction		;
		id_ex_jump_addr					<=	id_jump_addr			;
	end
end

endmodule