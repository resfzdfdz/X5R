//======================================================================
//  @Filename	:	bpu.v
//  @Data		:	2021.04.17
//	@Author		:	Li Xiaocong
//	@Department	:	School of microelectronic
//	@Version	:	1.0
//  @Abstract	:	static branch predictor 
//	@git		:	https://gitee.com/resfzdfdz/X5R.git
//
//	X5R is a basic 5 stage pipeline implementation of RISC-V ISA;
//	you can redistribute it and/or modify it under the terms of the GNU 
//	General Public License as published by the Free Software Foundation; 
//	either version 3, or (at your option) any later version.
//
//	X5R is distributed in the hope that it will be useful, but WITHOUT ANY
//	WARRANTY; without even the implied warranty of MERCHANTABILITY or
//	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
//	for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with X5R; see the file COPYING3.  If not see
//	<http://www.gnu.org/licenses/>.
//======================================================================
//	This module is static branch predictor:
//		For JAL:	always jump, jump address can be obtained in ID stage.
//		For JALR:	always jump, jump address can be obtained in EX stage.
//		For BXX:	predict jump when this branch is from back jump to front.
//					predict not jump when this branch is from front jump to back.
//
//		If jump is performed at stage n, stage 0 to stage n-1 must be flushed.
//======================================================================

module bpu
#(
	parameter XLEN = 32
)
(
	input wire				clk,
	input wire 				rst,
	input wire [1:0] 		id_jump_type,
	input wire 				id_branch_direction,
	input wire [XLEN-1:0]	id_jump_addr,
	
	input wire [1:0]		id_ex_jump_type,
	input wire 	[XLEN-1:0]	id_ex_jump_addr,
	
	input wire 				ex_predict_correct,
	input wire [XLEN-1:0]	ex_jalr_addr,
	input wire [XLEN-1:0]	ex_non_branch_addr,
	input wire				ex_branch_taken,

	output reg 				bpu_id_flush,
	output reg 				bpu_ex_flush,
	output reg [XLEN-1:0]	bpu_flush_pc
);
			
parameter	NOT_BRANCH	=	2'b00,
			BXX			=	2'b01,
			JAL			=	2'b10,
			JALR		=	2'b11;

//	Static Predictor and Flush Contorl
always @(*)
begin
	if (~ex_predict_correct)		//	mispredict flush
		if (ex_branch_taken) begin
			bpu_id_flush = 1'b0;
			bpu_ex_flush = 1'b1;
			bpu_flush_pc = id_ex_jump_addr;
		end
		else begin					//	mispredict flush
			bpu_id_flush = 1'b0;
			bpu_ex_flush = 1'b1;
			bpu_flush_pc = ex_non_branch_addr;
		end
	else if (id_ex_jump_type == JALR) begin	//	JALR flush
		bpu_id_flush = 1'b0;
		bpu_ex_flush = 1'b1;
		bpu_flush_pc = ex_jalr_addr;
	end
	else if (id_jump_type == BXX)	//	predict branch taken flush
		if (id_branch_direction) begin
			bpu_id_flush = 1'b1;
			bpu_ex_flush = 1'b0;
			bpu_flush_pc = id_jump_addr;
		end
		else begin
			bpu_id_flush = 1'b0;
			bpu_ex_flush = 1'b0;
			bpu_flush_pc = 32'h0;
		end
	else if (id_jump_type == JAL) begin	//	JAL flush
		bpu_id_flush = 1'b1;
		bpu_ex_flush = 1'b0;
		bpu_flush_pc = id_jump_addr;
	end
	else begin
		bpu_id_flush = 1'b0;
		bpu_ex_flush = 1'b0;
		bpu_flush_pc = 32'h0;
	end
end

endmodule

