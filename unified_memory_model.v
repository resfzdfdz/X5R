//======================================================================
//  @Filename	:	unified_mem_model.v
//  @Data		:	2021.04.30
//	@Author		:	Li Xiaocong
//	@Department	:	School of microelectronic
//	@Version	:	1.0
//  @Abstract	:	Unified Memory Model
//	@git		:	https://gitee.com/resfzdfdz/X5R.git
//
//	X5R is a basic 5 stage pipeline implementation of RISC-V ISA;
//	you can redistribute it and/or modify it under the terms of the GNU 
//	General Public License as published by the Free Software Foundation; 
//	either version 3, or (at your option) any later version.
//
//	X5R is distributed in the hope that it will be useful, but WITHOUT ANY
//	WARRANTY; without even the implied warranty of MERCHANTABILITY or
//	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
//	for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with X5R; see the file COPYING3.  If not see
//	<http://www.gnu.org/licenses/>.
//======================================================================
//	This is a unified memory model, instructions and data are placed
//	in this unified memory. I use this unified memory model because
//	gcc compiler's requirement.
//======================================================================

module unified_memory_model
#(
	parameter XLEN = 32
)
(
	input wire 				clk,
	input wire 				rst,
	input wire [XLEN-1:0] 	if_pc,
	output wire [XLEN-1:0] 	if_inst,
	output wire 			if_inst_align,
	
	input wire [2:0]		ls_type,
	input wire				re,
	input wire				we,
	input wire [XLEN-1:0]	addr,
	input wire [XLEN-1:0]	din,
	output reg [XLEN-1:0]	dout,
	output reg				addr_align
);

parameter	F3_LB		=	3'b000,
			F3_LH		=	3'b001,
			F3_LW		=	3'b010,
			F3_LBU		=	3'b100,
			F3_LHU		=	3'b101,
			F3_SB		=	3'b000,
			F3_SH		=	3'b001,
			F3_SW		=	3'b010;
			
reg [7:0] memory [1023:0];

//	For Instruction Fetch
wire [XLEN-1:0] pc0, pc1, pc2, pc3;
wire [7:0] ib0, ib1, ib2, ib3;

assign pc0 = if_pc;
assign pc1 = if_pc + 32'd1;
assign pc2 = if_pc + 32'd2;
assign pc3 = if_pc + 32'd3;

assign if_inst_align = (if_pc[1:0] == 2'b00);

assign ib0 = memory[pc0];
assign ib1 = memory[pc1];
assign ib2 = memory[pc2];
assign ib3 = memory[pc3];

assign if_inst = {ib3, ib2, ib1, ib0};

//	For Data Memory Access
//	Address Align detection
always @(*)
begin
	if (re | we)
		case (ls_type)
			//	If load/store byte, all address is aligned.
			F3_LB,
			F3_LBU,
			F3_SB:		addr_align = 1'b1;	
			//	If load/store half word, (addr[0] == 0) is aligned.
			F3_LH,
			F3_LHU,
			F3_SH:		addr_align = (addr[0] == 1'b0) ? 1'b1 : 1'b0;	
			//	If load/store word, (addr[1:0] == 2'b00) is aligned.
			F3_LW,
			F3_SW:		addr_align = (addr[1:0] == 2'b00) ? 1'b1 : 1'b0;
			default:	addr_align = 1'b1;
		endcase
	else
		addr_align = 1'b1;
end

wire [XLEN-1:0] da0, da1, da2, da3;
wire [7:0] db0, db1, db2, db3;

assign da0 = addr;
assign da1 = addr + 32'h1;
assign da2 = addr + 32'h2;
assign da3 = addr + 32'h3;

assign db0 = memory[da0];
assign db1 = memory[da1];
assign db2 = memory[da2];
assign db3 = memory[da3];

//	Memory Read
always @(*)
begin
	if (re)
		case (ls_type)
			F3_LB:		dout = { {(24){db0[7]}}, db0 };
			F3_LBU:		dout = { {(24){1'b0}}, db0 };
			F3_LH:		dout = { {(16){db1[7]}}, db1, db0 };
			F3_LHU:		dout = { {(16){1'b0}}, db1, db0 };
			F3_LW:		dout = { db3, db2, db1, db0 };
			default:	dout = 32'h0;
		endcase
	else
		dout = 32'h0;
end

//	Memory Write
integer i;

always @(posedge clk)
begin
	if (we)
		case (ls_type)
			F3_SB:		begin
							memory[da0] 	<= din[7:0];
						end
			F3_SH:		begin
							memory[da0] 	<= din[7:0];
							memory[da1]		<= din[15:8];
						end
			F3_SW:		begin
							memory[da0]		<= din[7:0];
							memory[da1]		<= din[15:8];
							memory[da2]		<= din[23:16];
							memory[da3]		<= din[31:24];
						end
		endcase
end

endmodule