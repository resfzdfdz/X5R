//======================================================================
//  @Filename	:	mem.v
//  @Data		:	2021.04.11
//	@Author		:	Li Xiaocong
//	@Department	:	School of microelectronic
//	@Version	:	1.0
//  @Abstract	:	MEM stage
//	@git		:	https://gitee.com/resfzdfdz/X5R.git
//
//	X5R is a basic 5 stage pipeline implementation of RISC-V ISA;
//	you can redistribute it and/or modify it under the terms of the GNU 
//	General Public License as published by the Free Software Foundation; 
//	either version 3, or (at your option) any later version.
//
//	X5R is distributed in the hope that it will be useful, but WITHOUT ANY
//	WARRANTY; without even the implied warranty of MERCHANTABILITY or
//	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
//	for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with X5R; see the file COPYING3.  If not see
//	<http://www.gnu.org/licenses/>.
//======================================================================
//	This module is mem stage
//======================================================================

module mem
#(
	parameter XLEN = 32
)
(
	input wire 				clk,
	input wire				rst,
	input wire [2:0]		ex_mem_ls_type,
	input wire 				ex_mem_memory_re,
	input wire 				ex_mem_memory_we,
	input wire [XLEN-1:0]	ex_mem_rd_value,
	input wire [XLEN-1:0]	ex_mem_data_need_store,
	
	output wire	[XLEN-1:0]	mem_memory_dout,
	output wire 			mem_addr_align,
	
	output wire [2:0]		ls_type,
	output wire 			re,
	output wire				we,
	output wire	[XLEN-1:0]	addr,
	output wire	[XLEN-1:0]	din,
	input wire [XLEN-1:0]	dout,
	input wire 				addr_align
);

assign ls_type = ex_mem_ls_type;
assign re = ex_mem_memory_re;
assign we = ex_mem_memory_we;
assign addr = ex_mem_rd_value;
assign din = ex_mem_data_need_store;

/*
data_mem_model u_data_mem_model
(
	.clk			(	clk						),
	.rst			(	rst						),
	.ls_type		(	ex_mem_ls_type			),
	.re				(	ex_mem_memory_re		),
	.we				(	ex_mem_memory_we		),
	.addr			(	ex_mem_rd_value			),
	.din			(	ex_mem_data_need_store	),
	.dout			(	mem_dout				),
	.addr_align		(	addr_align				)
);
*/

assign mem_memory_dout = dout;
assign mem_addr_align = addr_align;

//	Exception will be resolved here.


endmodule


