//======================================================================
//  @Filename	:	ShiftUnit.sv
//  @Data		:	2021.02.18
//	@Author		:	Li Xiaocong
//	@Department	:	School of microelectronic
//	@Version	:	1.0
//  @Abstract	:	Shift Unit for X5R
//	@git		:	https://gitee.com/resfzdfdz/X5R.git
//
//	X5R is free Xi'an Jiaotong University RISC-V out-of-order machine; 
//	you can redistribute it and/or modify it under the terms of the GNU 
//	General Public License as published by the Free Software Foundation; 
//	either version 3, or (at your option) any later version.
//
//	X5R is distributed in the hope that it will be useful, but WITHOUT ANY
//	WARRANTY; without even the implied warranty of MERCHANTABILITY or
//	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
//	for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with X5R; see the file COPYING3.  If not see
//	<http://www.gnu.org/licenses/>.
//======================================================================
//	This module include below 6 instructions 
//	SLLI
//	SRLI
//	SRAI
//	SLL
//	SRL
//	SRA
//======================================================================

module ShiftUnit
#(
	parameter XLEN = 32
)
(
	input logic [1:0] 		ShiftCode,
	input logic [4:0] 		ShiftNum,
	input logic [XLEN-1:0] 	Number,
	output logic [XLEN-1:0] ShiftUnitRes
);

parameter	SHIFT_LEFT				=	2'b00,
			SHIFT_UNSIGNED_RIGHT	=	2'b10,
			SHIFT_SIGNED_RIGHT		=	2'b11;
			
always_comb begin
	case (ShiftCode)
		SHIFT_LEFT:				ShiftUnitRes	=	Number << ShiftNum;
		SHIFT_UNSIGNED_RIGHT:	ShiftUnitRes	=	Number >> ShiftNum;
		SHIFT_SIGNED_RIGHT:		ShiftUnitRes	=	($signed(Number)) >>> ShiftNum;
		default:				ShiftUnitRes	=	Number;
	endcase
end

endmodule
			
			