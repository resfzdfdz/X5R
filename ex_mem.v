//======================================================================
//  @Filename	:	ex_mem.v
//  @Data		:	2021.04.11
//	@Author		:	Li Xiaocong
//	@Department	:	School of microelectronic
//	@Version	:	1.0
//  @Abstract	:	EX/MEM Pipeline wireister
//	@git		:	https://gitee.com/resfzdfdz/X5R.git
//
//	X5R is a basic 5 stage pipeline implementation of RISC-V ISA;
//	you can redistribute it and/or modify it under the terms of the GNU 
//	General Public License as published by the Free Software Foundation; 
//	either version 3, or (at your option) any later version.
//
//	X5R is distributed in the hope that it will be useful, but WITHOUT ANY
//	WARRANTY; without even the implied warranty of MERCHANTABILITY or
//	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
//	for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with X5R; see the file COPYING3.  If not see
//	<http://www.gnu.org/licenses/>.
//======================================================================
//	This module is EX/MEM Pipeline register
//======================================================================

module ex_mem
#(
	parameter XLEN = 32
)
(
	input wire 				clk,
	input wire 				rst,
	input wire 				ex_mem_stall,
	input wire 				ex_mem_flush,
	//	Input from ID/EX 
	//	For register control
	input wire [4:0]		id_ex_rd_num,
	input wire 				id_ex_rd_we,	
	input reg [4:0]			id_ex_rs1_num,
	input reg				id_ex_rs1_re,
	input reg [4:0]			id_ex_rs2_num,
	input reg				id_ex_rs2_re,
	output reg [4:0]		ex_mem_rd_num,
	output reg 				ex_mem_rd_we,
	output reg [4:0]		ex_mem_rs1_num,
	output reg				ex_mem_rs1_re,
	output reg [4:0]		ex_mem_rs2_num,
	output reg				ex_mem_rs2_re,
	//	For memory control
	input reg [2:0]			id_ex_ls_type,
	input reg 				id_ex_memory_re,
	input reg 				id_ex_memory_we,
	output reg [2:0]		ex_mem_ls_type,
	output reg 				ex_mem_memory_re,
	output reg 				ex_mem_memory_we,

	//	Input from EX 
	input wire [XLEN-1:0]	ex_rd_value,
	input wire [XLEN-1:0]	ex_data_need_store,
	output reg [XLEN-1:0]	ex_mem_rd_value,
	output reg [XLEN-1:0]	ex_mem_data_need_store

);


always @(posedge clk)
begin
	if (rst) begin
		ex_mem_rd_num			<=	0;
		ex_mem_rd_we			<=	0;
		ex_mem_ls_type			<=	0;
		ex_mem_memory_re		<=	0;
		ex_mem_memory_we		<=	0;
		ex_mem_rd_value			<=	0;
		ex_mem_data_need_store	<=	0;
		ex_mem_rs1_num			<=	0;
		ex_mem_rs1_re			<=	0;
		ex_mem_rs2_num			<=	0;
		ex_mem_rs2_re			<=	0;
	end
	else if (ex_mem_flush) begin
		ex_mem_rd_num			<=	0;
		ex_mem_rd_we			<=	0;
		ex_mem_ls_type			<=	0;
		ex_mem_memory_re		<=	0;
		ex_mem_memory_we		<=	0;
		ex_mem_rd_value			<=	0;
		ex_mem_data_need_store	<=	0;
		ex_mem_rs1_num			<=	0;
		ex_mem_rs1_re			<=	0;
		ex_mem_rs2_num			<=	0;
		ex_mem_rs2_re			<=	0;
	end
	else if (ex_mem_stall) begin
		ex_mem_rd_num			<=	ex_mem_rd_num			;
		ex_mem_rd_we			<=	ex_mem_rd_we			;
		ex_mem_ls_type			<=	ex_mem_ls_type			;
		ex_mem_memory_re		<=	ex_mem_memory_re		;
		ex_mem_memory_we		<=	ex_mem_memory_we		;
		ex_mem_rd_value			<=	ex_mem_rd_value			;
		ex_mem_data_need_store	<=	ex_mem_data_need_store	;
		ex_mem_rs1_num			<=	ex_mem_rs1_num			;
		ex_mem_rs1_re			<=	ex_mem_rs1_re			;
		ex_mem_rs2_num			<=	ex_mem_rs2_num			;
		ex_mem_rs2_re			<=	ex_mem_rs2_re			;
	end
	else begin
		ex_mem_rd_num			<=	id_ex_rd_num;
		ex_mem_rd_we			<=	id_ex_rd_we;
		ex_mem_ls_type			<=	id_ex_ls_type;
		ex_mem_memory_re		<=	id_ex_memory_re;
		ex_mem_memory_we		<=	id_ex_memory_we;
		ex_mem_rd_value			<=	ex_rd_value;
		ex_mem_data_need_store	<=	ex_data_need_store;
		ex_mem_rs1_num			<=	id_ex_rs1_num;
		ex_mem_rs1_re			<=	id_ex_rs1_re;
		ex_mem_rs2_num			<=	id_ex_rs2_num;
		ex_mem_rs2_re			<=	id_ex_rs2_re;
	end
end


endmodule