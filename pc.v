//======================================================================
//  @Filename	:	PC.v
//  @Data		:	2021.04.11
//	@Author		:	Li Xiaocong
//	@Department	:	School of microelectronic
//	@Version	:	1.0
//  @Abstract	:	Program Counter
//	@git		:	https://gitee.com/resfzdfdz/X5R.git
//
//	X5R is a basic 5 stage pipeline implementation of RISC-V ISA;
//	you can redistribute it and/or modify it under the terms of the GNU 
//	General Public License as published by the Free Software Foundation; 
//	either version 3, or (at your option) any later version.
//
//	X5R is distributed in the hope that it will be useful, but WITHOUT ANY
//	WARRANTY; without even the implied warranty of MERCHANTABILITY or
//	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
//	for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with X5R; see the file COPYING3.  If not see
//	<http://www.gnu.org/licenses/>.
//======================================================================
//	This module is program counter. It can be
//		1. stall: pc holds
//		2. flush: pc change its value to flush address
//======================================================================

module pc
#(
	parameter XLEN = 32,
	parameter START_ADDR = 32'h0
)
(
	input wire 				clk,
	input wire 				rst,
	input wire 				pc_stall,
	input wire 				pc_flush,
	input wire [XLEN-1:0]	pc_flush_addr,
	
	output reg [XLEN-1:0] 	pc
);

always @(posedge clk)
begin
	if (rst)
		pc <= START_ADDR;
	else if (pc_flush)
		pc <= pc_flush_addr;
	else if (pc_stall)
		pc <= pc;
	else
		pc <= pc + 32'h4;
end


endmodule



