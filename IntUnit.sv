//======================================================================
//  @Filename	:	IntUnit.sv
//  @Data		:	2020.12.19
//	@Author		:	Li Xiaocong
//	@Department	:	School of microelectronic
//	@Version	:	1.0
//  @Abstract	:	Integer Unit for X5R
//	@git		:	https://gitee.com/resfzdfdz/X5R.git
//
//	X5R is free Xi'an Jiaotong University RISC-V out-of-order machine; 
//	you can redistribute it and/or modify it under the terms of the GNU 
//	General Public License as published by the Free Software Foundation; 
//	either version 3, or (at your option) any later version.
//
//	X5R is distributed in the hope that it will be useful, but WITHOUT ANY
//	WARRANTY; without even the implied warranty of MERCHANTABILITY or
//	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
//	for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with X5R; see the file COPYING3.  If not see
//	<http://www.gnu.org/licenses/>.
//======================================================================
//	This module include below (Instruction : Function Unit) 
//	ADDI / ADDI	/SUB				:	Addsub Unit
//	SLTI / SLTIU / SLT / SLTU  		:	
//	BLT / BGE / BLTU / BGEU			:	Compare Unit
//	XOR / XORI						:	
//	AND / ANDI						:	
//	OR / ORI						:	Bit Unit
//======================================================================
module IntUnit
#(
	parameter XLEN = 32
)
(
	input logic [XLEN-1:0] Vj,
	input logic [XLEN-1:0] Vk,
	input logic [2:0] IntUnitCode,
	input logic [1:0] ctrl,
	output logic [XLEN-1:0] IntUnitRes
);

//	Definition of IntUnitCode
parameter 	ADDSUB	=	3'b000,
			LT		=	3'b001,
			GE		=	3'b010,
			EQ		=	3'b011,
			NE		=	3'b100,
			XOR		=	3'b101,
			AND		=	3'b110,
			OR		=	3'b111;

//	Addsub Unit
//	ctrl[0] = 1'b0 --> res_addsub = Vj + Vk;
//	ctrl[0] = 1'b1 --> res_addsub = Vj + (~Vk) + 1 = Vj - Vk;
logic [XLEN-1:0] res_addsub;
assign res_addsub = Vj + ({(XLEN){ctrl[0]}} ^ Vk) + ctrl[0];

//	Compare Unit
//	ctrl[1] = 1'b0 --> unsigned compare
//	ctrl[1] = 1'b1 --> signed compare
logic [XLEN:0] extended_Vj;
logic [XLEN:0] extended_Vk;
logic [XLEN:0] temp_sub;
logic lt, ge, eq, ne;

//	if comparing unsigned numbers, extend Vj and Vk with 1'b0.
//	if comparing signed numbers, extend Vj and Vk with signed bit
//	Vj[31] and Vk[31].
assign extended_Vj = {Vj[31] & ctrl[1] , Vj};
assign extended_Vk = {Vk[31] & ctrl[1] , Vk};
assign temp_sub = extended_Vj - extended_Vk;

//	(temp_sub[32] == 1'b1) means (temp_sub < 0) thus (Vj < Vk)
assign lt = temp_sub[32];
//	(temp_sub[32] == 1'b0) means (temp_sub >= 0) thus (Vj >= Vk)
assign ge = ~temp_sub[32];
//	(temp_sub == 33'h0) means (Vj = Vk)
assign eq = (temp_sub == 33'h0);
//	(temp_sub != 33'h0) means (Vj != Vk)
assign ne = ~eq;

//	Bit Unit
logic [XLEN-1:0] res_xor;
logic [XLEN-1:0] res_and;
logic [XLEN-1:0] res_or;

assign res_xor = Vj ^ Vk;
assign res_and = Vj & Vk;
assign res_or  = Vj | Vk;

//	Use IntUnitCode to select output 
logic [XLEN-1:0] output_value;

always_comb begin
	case (IntUnitCode)
		ADDSUB	:	output_value	=	res_addsub;
		LT		:	output_value	=	{31'h0, lt};
		GE		:	output_value	=	{31'h0, ge};
		EQ		:	output_value	=	{31'h0, eq};
		NE		:	output_value	=	{31'h0, ne};
		XOR		:	output_value	=	res_xor;
		AND		:	output_value	=	res_and;
		OR		:	output_value	=	res_or;
	endcase
end

assign IntUnitRes = output_value;

endmodule