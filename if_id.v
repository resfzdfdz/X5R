//======================================================================
//  @Filename	:	if_id.v
//  @Data		:	2021.04.11
//	@Author		:	Li Xiaocong
//	@Department	:	School of microelectronic
//	@Version	:	1.0
//  @Abstract	:	IF/ID Pipeline Register
//	@git		:	https://gitee.com/resfzdfdz/X5R.git
//
//	X5R is a basic 5 stage pipeline implementation of RISC-V ISA;
//	you can redistribute it and/or modify it under the terms of the GNU 
//	General Public License as published by the Free Software Foundation; 
//	either version 3, or (at your option) any later version.
//
//	X5R is distributed in the hope that it will be useful, but WITHOUT ANY
//	WARRANTY; without even the implied warranty of MERCHANTABILITY or
//	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
//	for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with X5R; see the file COPYING3.  If not see
//	<http://www.gnu.org/licenses/>.
//======================================================================
//	This module is IF/ID pipeline register
//======================================================================

module if_id
#(
	parameter XLEN = 32
)
(
	input wire clk,
	input wire rst,
	input wire if_id_stall,
	input wire if_id_flush,
	input wire [XLEN-1:0] if_pc,
	input wire [XLEN-1:0] if_inst,
	input wire if_inst_align,
	
	output reg [XLEN-1:0] if_id_pc,
	output reg [XLEN-1:0] if_id_inst,
	output reg if_id_align
);


always @(posedge clk)
begin
	if (rst) begin
		if_id_pc		<=	32'h0;
	    if_id_inst		<=	32'h0;
	    if_id_align		<=	1'h0;
	end
	else if (if_id_flush) begin
		if_id_pc		<=	0;
	    if_id_inst		<=	0;
	    if_id_align		<=	0;
	end
	else if (if_id_stall) begin
		if_id_pc		<=	if_id_pc;
	    if_id_inst		<=	if_id_inst;
	    if_id_align		<=	if_id_align;
	end
	else begin
		if_id_pc		<=	if_pc;
	    if_id_inst		<=	if_inst;
	    if_id_align		<=	if_inst_align;
	end
end
		
	
endmodule
	