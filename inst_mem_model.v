//======================================================================
//  @Filename	:	inst_mem_model.v
//  @Data		:	2021.04.11
//	@Author		:	Li Xiaocong
//	@Department	:	School of microelectronic
//	@Version	:	1.0
//  @Abstract	:	Instruction Memory Model
//	@git		:	https://gitee.com/resfzdfdz/X5R.git
//
//	X5R is a basic 5 stage pipeline implementation of RISC-V ISA;
//	you can redistribute it and/or modify it under the terms of the GNU 
//	General Public License as published by the Free Software Foundation; 
//	either version 3, or (at your option) any later version.
//
//	X5R is distributed in the hope that it will be useful, but WITHOUT ANY
//	WARRANTY; without even the implied warranty of MERCHANTABILITY or
//	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
//	for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with X5R; see the file COPYING3.  If not see
//	<http://www.gnu.org/licenses/>.
//======================================================================
//	This module is instruction memory model, it is not a 
//	real memory device. It is little-endian.
//		
//	Note: Instruction memory is read-only.
//
//	*********************************************************
//	Modified by Li Xiaocong in 2021.04.30:
//		Use unified memory model instead of seperate inst_mem
//		and data_mem.
//	*********************************************************
//======================================================================

module inst_mem_model
#(
	parameter XLEN = 32
)
(
	input wire [XLEN-1:0] 	if_pc,
	output wire [XLEN-1:0] 	if_inst,
	output wire 			if_inst_align
);

reg [7:0] memory [511:0];

wire [XLEN-1:0] addr0, addr1, addr2, addr3;
reg [7:0] byte0, byte1, byte2, byte3;

assign addr0 = {if_pc[XLEN-1:2], 2'b00};
assign addr1 = {if_pc[XLEN-1:2], 2'b01};
assign addr2 = {if_pc[XLEN-1:2], 2'b10};
assign addr3 = {if_pc[XLEN-1:2], 2'b11};

assign if_inst_align = (if_pc[1:0] == 2'b00);

always @(*)
begin
	byte0 = memory[addr0];
	byte1 = memory[addr1];
	byte2 = memory[addr2];
	byte3 = memory[addr3];
end

assign if_inst = {byte3, byte2, byte1, byte0};

endmodule

