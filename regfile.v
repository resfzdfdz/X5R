//======================================================================
//  @Filename	:	regfile.v
//  @Data		:	2021.04.11
//	@Author		:	Li Xiaocong
//	@Department	:	School of microelectronic
//	@Version	:	1.0
//  @Abstract	:	Register File
//	@git		:	https://gitee.com/resfzdfdz/X5R.git
//
//	X5R is a basic 5 stage pipeline implementation of RISC-V ISA;
//	you can redistribute it and/or modify it under the terms of the GNU 
//	General Public License as published by the Free Software Foundation; 
//	either version 3, or (at your option) any later version.
//
//	X5R is distributed in the hope that it will be useful, but WITHOUT ANY
//	WARRANTY; without even the implied warranty of MERCHANTABILITY or
//	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
//	for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with X5R; see the file COPYING3.  If not see
//	<http://www.gnu.org/licenses/>.
//======================================================================
//	This module is Register File
//		RISC-V registers:
//			x0(zero) 		: Constant 0 register
//			x1(ra)	 		: Return address register
//			x2(sp)	 		: Stack pointer (This register may be initialized to 
//								max address of data memory)
//			x3(gp)	 		: Global pointer
//			x4(tp)	 		: Thread pointer
//			x5-7(t0-2)		: Temporaries
//			x8(s0/fp)		: Saved register/frame pointer
//			x9(s1)			: Saved Register
//			x10-11(a0-1)	: Function arguments / return value
//			x12-17(a2-7)	: Function arguments
//			x18-27(s2-11)	: Saved registers
//			x28-31(t3-6)	: Temporaries
//			
//	Note : rs2 and rs1 can bypass from rd when
//		(rsx_re == 1) && (rd_we == 1) && (rsx_num == rd_num)
//======================================================================

module regfile
#(
	parameter XLEN = 32,
	parameter RLEN = 32
)
(
	input wire 				clk,
	input wire 				rst,
	//	Register Read Port rs2
	input wire 				rs2_re,
	input wire [4:0] 		rs2_num,
	output reg [XLEN-1:0] 	rs2_value,
	//	Register Read Port rs1
	input wire 				rs1_re,
	input wire [4:0] 		rs1_num,
	output reg [XLEN-1:0] 	rs1_value,
	//	Register Write Port rd
	input wire 				rd_we,
	input wire [4:0]		rd_num,
	input wire [XLEN-1:0]	rd_value
);

reg [XLEN-1:0] regfile [RLEN-1:0];

//	Port rs2 can bypass from Port rd
always @(*) begin
	if (rs1_re && rd_we && (rs1_num == rd_num) )
		rs1_value = rd_value;
	else if (rs1_re)
		rs1_value = regfile[rs1_num];
	else
		rs1_value = 32'h0;
end

//	Port rs2 can bypass from Port rd
always @(*) begin
	if (rs2_re && rd_we && (rs2_num == rd_num) )
		rs2_value = rd_value;
	else if (rs2_re)
		rs2_value = regfile[rs2_num];
	else
		rs2_value = 32'h0;
end

integer i;

//	Port rd
always @(posedge clk)
begin
	if (rst) begin
		for (i = 0; i < RLEN; i=i+1)
			if (i == 2)
				regfile[i] <= 32'h400; // stack point(sp) is initialized at the top of the memory, this memory model is 256 byte.
			else
				regfile[i] <= 'h0;
	end
	else
		if (rd_we && (rd_num != 32'h0))
			regfile[rd_num] <= rd_value;
end

endmodule


