
@echo off
rd/s/q lib
del modelsim.ini
del auto_sim.log
del vsim.wlf

vsim -do auto_sim.do -l auto_sim.log -wlf auto_sim.wlf

rem pause>nul