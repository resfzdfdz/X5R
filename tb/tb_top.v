//======================================================================
//  @Filename	:	tb_top.v
//  @Data		:	2021.04.11
//	@Author		:	Li Xiaocong
//	@Department	:	School of microelectronic
//	@Version	:	1.0
//  @Abstract	:	Testbench for top module
//	@git		:	https://gitee.com/resfzdfdz/X5R.git
//
//	X5R is a basic 5 stage pipeline implementation of RISC-V ISA;
//	you can redistribute it and/or modify it under the terms of the GNU 
//	General Public License as published by the Free Software Foundation; 
//	either version 3, or (at your option) any later version.
//
//	X5R is distributed in the hope that it will be useful, but WITHOUT ANY
//	WARRANTY; without even the implied warranty of MERCHANTABILITY or
//	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
//	for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with X5R; see the file COPYING3.  If not see
//	<http://www.gnu.org/licenses/>.
//======================================================================
//	This module is testbench for top module:
//		We use a short program to verify the simple RISC-V core
//======================================================================

module tb_top;

reg clk = 0;
reg rst = 0;

parameter START_ADDR = 32'h38;

top 
#(
	.START_ADDR(START_ADDR)
) 
u_top
(
	.clk	(	clk		),
	.rst	(	rst		)
);

parameter HP = 5;

always #(HP) clk = ~clk;

initial begin
	rst <= 1;
	#(HP * 10) @(posedge clk)
	rst <= 0;
end

//	C:/Users/lxc/Desktop/icon/riscv/new/tb/bin.txt
//	C:/Users/nothi/Desktop/X5R/tb/program/
initial begin
	$readmemh ("C:/Users/nothi/Desktop/X5R/tb/program/bin3.txt", u_top.u_unified_memory_model.memory);
end

endmodule