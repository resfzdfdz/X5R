#quit -sim                        
#.main    clear
vlib    ./lib
vlib    ./lib/work
vmap     work ./lib/work


vlog	-work	 work	 ./*.v
vlog	-work	 work	 ./../*.v
vlog	-work	 work	 ./../*.sv

#vlog	-work	 work	 -cover bcest 	./../*.sv
#vlog	-work	 work	 -cover bcest 	./../*.v
#vlog	-work	 work	 -cover bcest 	./../../*.sv
#vlog	-work	 work	 -cover bcest  	./../../*.v


vsim    -voptargs=+acc    work.tb_top

#vsim	-coverage -novopt 	work.tb_top  work.top_fsm

add		wave	-hex	tb_top/u_top/*

#add    wave    -divider     {task signals}

#add	   wave    -hex         tb_top/send_inst/*
#add	   wave    -hex         tb_top/test_alu_fp/*

#add    wave    -divider     {top signals}




run    -all

#coverage report -recursive -select bces -file coverage.log