//======================================================================
//  @Filename	:	top.v
//  @Data		:	2021.04.11
//	@Author		:	Li Xiaocong
//	@Department	:	School of microelectronic
//	@Version	:	1.0
//  @Abstract	:	top module
//	@git		:	https://gitee.com/resfzdfdz/X5R.git
//
//	X5R is a basic 5 stage pipeline implementation of RISC-V ISA;
//	you can redistribute it and/or modify it under the terms of the GNU 
//	General Public License as published by the Free Software Foundation; 
//	either version 3, or (at your option) any later version.
//
//	X5R is distributed in the hope that it will be useful, but WITHOUT ANY
//	WARRANTY; without even the implied warranty of MERCHANTABILITY or
//	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
//	for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with X5R; see the file COPYING3.  If not see
//	<http://www.gnu.org/licenses/>.
//======================================================================
//	This module is top module of RISC-V 5 stage pipeline
//======================================================================

module top
#(
	parameter XLEN = 32,
	parameter START_ADDR = 32'h0
)
(
	input wire clk,
	input wire rst
);

wire [XLEN-1:0] pc, 
				if_inst,
				if_id_pc,
				if_id_inst,
				id_imm,
				id_ex_pc,
				id_ex_imm,
				id_rs2_value,
				id_rs1_value,
				id_ex_rs1_value,
				id_ex_rs2_value,
				ex_mem_rd_value,
				mem_wb_rd_value,
				ex_pc,
				ex_rd_value,
				wb_rd_value;

wire 	if_inst_align,
		if_id_align,
		id_inst_valid,	
		id_mux_vj,		
		id_mux_vk,		
		id_rd_we,
		id_rs1_re,
		id_rs2_re,
		id_ex_align,
		id_ex_inst_valid,
		id_ex_mux_vj,	
		id_ex_mux_vk,
		id_ex_rd_we,
		id_ex_rs1_re,
		id_ex_rs2_re,
		ex_mem_rd_we,
		mem_wb_rd_we,
		ex_align;
		
wire [2:0] id_mux_output, id_ex_mux_output;

wire [2:0] int_unit_type_word, id_ex_int_unit_type_word;
wire [1:0] int_unit_ctrl_word, id_ex_int_unit_ctrl_word;
wire [1:0] shift_unit_ctrl_word, id_ex_shift_unit_ctrl_word;

wire [4:0] 	id_rd_num,
			id_rs1_num,
			id_rs2_num,
			id_ex_rd_num,
			id_ex_rs1_num,
			id_ex_rs2_num,
			ex_mem_rd_num,
			mem_wb_rd_num,
			wb_rd_num;
			
wire [4:0] 	stall,
			flush;
			
wire [2:0]		id_ls_type;
wire 			id_memory_re;
wire 			id_memory_we;

wire [2:0]		id_ex_ls_type;
wire 			id_ex_memory_re;
wire 			id_ex_memory_we;

wire [XLEN-1:0]	ex_data_need_store;
wire 			ex_stall_for_lr_hazard;

wire [2:0]		ex_mem_ls_type;
wire 			ex_mem_memory_re;
wire 			ex_mem_memory_we;
wire [XLEN-1:0]	ex_mem_data_need_store;

wire[XLEN-1:0]	mem_memory_dout;
wire 			mem_addr_align;

wire			mem_wb_memory_re;
wire [XLEN-1:0]	mem_wb_memory_dout;
wire			mem_wb_addr_align;

wire [4:0]		ex_mem_rs1_num;
wire			ex_mem_rs1_re;
wire [4:0]		ex_mem_rs2_num;
wire			ex_mem_rs2_re;

wire			id_mux_auipc;
wire 			id_ex_mux_auipc;

wire [XLEN-1:0]	pc_flush_addr;
wire 			pc_branch_taken;
wire [XLEN-1:0]	pc_branch_addr;

wire [1:0]		id_jump_type;
wire [2:0]		id_branch_type;
wire			id_branch_direction;
wire [XLEN-1:0]	id_jump_addr;

wire [1:0]		id_ex_jump_type;
wire [2:0]		id_ex_branch_type;
wire 			id_ex_branch_direction;
wire [XLEN-1:0]	id_ex_jump_addr	;

wire 			ex_predict_correct;
wire [XLEN-1:0]	ex_jalr_addr;

wire [4:0]		mem_wb_rs1_num;
wire			mem_wb_rs1_re;
wire [4:0]		mem_wb_rs2_num;
wire			mem_wb_rs2_re;

wire [XLEN-1:0]	ex_non_branch_addr;

wire 			bpu_id_flush;
wire 			bpu_ex_flush;
wire [XLEN-1:0]	bpu_flush_pc;

wire [2:0]		ls_type;
wire 			re;
wire			we;
wire [XLEN-1:0]	addr;
wire [XLEN-1:0]	din;
wire [XLEN-1:0]	dout;
wire 			addr_align;

pc 
#(	
	.START_ADDR	(START_ADDR)
)
u_pc
(
	.clk			(	clk				),
	.rst			(	rst				),
	.pc_stall		(	stall[0]		),
	.pc_flush		(	flush[0]		),
	.pc_flush_addr	(	bpu_flush_pc	),
	.pc				(	pc				)
);

/*
inst_mem_model u_inst_mem_model
(
	.if_pc			(	pc				),
	.if_inst		(	if_inst			),
	.if_inst_align	(	if_inst_align	)
);
*/

if_id u_if_id
(
	.clk			(	clk				),
	.rst			(	rst				),
	.if_id_stall	(	stall[1]		),
	.if_id_flush	(	flush[1]		),
	.if_pc			(	pc				),
	.if_inst		(	if_inst			),
	.if_inst_align	(	if_inst_align	),
	.if_id_pc		(	if_id_pc		),
	.if_id_inst		(	if_id_inst		),
	.if_id_align	(	if_id_align		)
);

decoder u_decoder
(
	.id_pc					(	if_id_pc			),
	.id_inst				(	if_id_inst			),
	.id_ex_rd_we			(	id_ex_rd_we			),
	.id_ex_rd_num			(	id_ex_rd_num		),
	.id_ex_memory_re		(	id_ex_memory_re		),
	.int_unit_type_word		(	int_unit_type_word	),
	.int_unit_ctrl_word		(	int_unit_ctrl_word	),
	.shift_unit_ctrl_word	(	shift_unit_ctrl_word),
	.id_imm					(	id_imm				),
	.id_inst_valid			(	id_inst_valid		),
	.id_mux_vj				(	id_mux_vj			),
	.id_mux_vk				(	id_mux_vk			),
	.id_mux_auipc			(	id_mux_auipc		),
	.id_mux_output			(	id_mux_output		),
	.id_ls_type				(	id_ls_type			),
	.id_memory_re			(	id_memory_re		),
	.id_memory_we			(	id_memory_we		),
	.id_rd_num				(	id_rd_num			),
	.id_rd_we				(	id_rd_we			),
	.id_rs1_num				(	id_rs1_num			),
	.id_rs1_re				(	id_rs1_re			),
	.id_rs2_num				(	id_rs2_num			),
	.id_rs2_re				(	id_rs2_re			),
	.id_stall_for_lr_hazard (	id_stall_for_lr_hazard),
	.id_jump_type			(	id_jump_type		),
	.id_branch_direction	(	id_branch_direction	),
	.id_jump_addr			(	id_jump_addr		)
);

regfile u_regfile
(
	.clk					(	clk					),
	.rst					(	rst					),
	.rs2_re					(	id_rs2_re			),
	.rs2_num				(	id_rs2_num			),
	.rs2_value				(	id_rs2_value		),
	.rs1_re					(	id_rs1_re			),
	.rs1_num				(	id_rs1_num			),
	.rs1_value				(	id_rs1_value		),
	.rd_we					(	wb_rd_we			),
	.rd_num					(	wb_rd_num			),
	.rd_value				(	wb_rd_value			)
);

id_ex u_id_ex
(
	.clk					(	clk						),
	.rst					(	rst						),
	.id_ex_stall			(	stall[2]				),
	.id_ex_flush			(	flush[2]				),
	.id_pc					(	if_id_pc				),
	.id_align				(	if_id_align				),
	.int_unit_type_word		(	int_unit_type_word		),
	.int_unit_ctrl_word		(	int_unit_ctrl_word		),
	.shift_unit_ctrl_word	(	shift_unit_ctrl_word	),
	.id_imm					(	id_imm					),
	.id_inst_valid			(	id_inst_valid			),
	.id_mux_vj				(	id_mux_vj				),
	.id_mux_vk				(	id_mux_vk				),
	.id_mux_auipc			(	id_mux_auipc			),
	.id_mux_output			(	id_mux_output			),
	.id_ls_type				(	id_ls_type				),
	.id_memory_re			(	id_memory_re			),
	.id_memory_we			(	id_memory_we			),
	.id_rd_num				(	id_rd_num				),
	.id_rd_we				(	id_rd_we				),
	.id_rs1_num				(	id_rs1_num				),
	.id_rs1_re				(	id_rs1_re				),
	.id_rs2_num				(	id_rs2_num				),
	.id_rs2_re				(	id_rs2_re				),
	.id_rs1_value			(	id_rs1_value			),
	.id_rs2_value			(	id_rs2_value			),
	.id_stall_for_lr_hazard	(	id_stall_for_lr_hazard	),
	.id_jump_type			(	id_jump_type			),
	.id_branch_direction	(	id_branch_direction		),
	.id_jump_addr			(	id_jump_addr			),
	
	.id_ex_pc				(	id_ex_pc				),
	.id_ex_align			(	id_ex_align				),
	.id_ex_int_unit_type_word(	id_ex_int_unit_type_word),
	.id_ex_int_unit_ctrl_word(	id_ex_int_unit_ctrl_word),
	.id_ex_shift_unit_ctrl_word(id_ex_shift_unit_ctrl_word),
	.id_ex_imm				(	id_ex_imm				),
	.id_ex_inst_valid		(	id_ex_inst_valid		),
	.id_ex_mux_vj			(	id_ex_mux_vj			),
	.id_ex_mux_vk			(	id_ex_mux_vk			),
	.id_ex_mux_auipc		(	id_ex_mux_auipc			),
	.id_ex_mux_output		(	id_ex_mux_output		),
	.id_ex_ls_type			(	id_ex_ls_type			),
	.id_ex_memory_re		(	id_ex_memory_re			),
	.id_ex_memory_we		(	id_ex_memory_we			),
	.id_ex_rd_num			(	id_ex_rd_num			),
	.id_ex_rd_we			(	id_ex_rd_we				),
	.id_ex_rs1_num			(	id_ex_rs1_num			),
	.id_ex_rs1_re			(	id_ex_rs1_re			),
	.id_ex_rs2_num			(	id_ex_rs2_num			),
	.id_ex_rs2_re			(	id_ex_rs2_re			),
	.id_ex_rs1_value		(	id_ex_rs1_value			),
	.id_ex_rs2_value		(	id_ex_rs2_value			),
	.id_ex_jump_type		(	id_ex_jump_type			),
	.id_ex_branch_direction	(	id_ex_branch_direction	),
	.id_ex_jump_addr		(	id_ex_jump_addr			)
);

ex u_ex
(
	.id_ex_pc				(	id_ex_pc				),
	.id_ex_align			(	id_ex_align				),
	.id_ex_int_unit_type_word(id_ex_int_unit_type_word	),
	.id_ex_int_unit_ctrl_word(id_ex_int_unit_ctrl_word	),
	.id_ex_shift_unit_ctrl_word(id_ex_shift_unit_ctrl_word),
	.id_ex_imm				(	id_ex_imm				),
	.id_ex_inst_valid		(	id_ex_inst_valid		),
	.id_ex_mux_vj			(	id_ex_mux_vj			),
	.id_ex_mux_vk			(	id_ex_mux_vk			),
	.id_ex_mux_auipc		(	id_ex_mux_auipc			),
	.id_ex_mux_output		(	id_ex_mux_output		),
	.id_ex_ls_type			(	id_ex_ls_type			),
	.id_ex_memory_re		(	id_ex_memory_re			),
	.id_ex_memory_we		(	id_ex_memory_we			),
	.id_ex_rd_num			(	id_ex_rd_num			),
	.id_ex_rd_we			(	id_ex_rd_we				),
	.id_ex_rs1_num			(	id_ex_rs1_num			),
	.id_ex_rs1_re			(	id_ex_rs1_re			),
	.id_ex_rs2_num			(	id_ex_rs2_num			),
	.id_ex_rs2_re			(	id_ex_rs2_re			),
	.id_ex_rs1_value		(	id_ex_rs1_value			),
	.id_ex_rs2_value		(	id_ex_rs2_value			),
	.ex_mem_rd_num			(	ex_mem_rd_num			),
	.ex_mem_rd_we			(	ex_mem_rd_we			),
	.ex_mem_rd_value		(	ex_mem_rd_value			),
	.mem_wb_rd_num			(	mem_wb_rd_num			),
	.mem_wb_rd_we			(	mem_wb_rd_we			),
	.mem_wb_rd_value		(	mem_wb_rd_value			),
	.ex_mem_memory_re		(	ex_mem_memory_re		),
	.ex_mem_memory_we		(	ex_mem_memory_we		),
	.wb_rd_value			(	wb_rd_value				),
	.id_ex_jump_type		(	id_ex_jump_type			),
	.id_ex_branch_direction	(	id_ex_branch_direction	),
	.id_ex_jump_addr		(	id_ex_jump_addr			),
	
	.ex_rd_value			(	ex_rd_value				),
	.ex_data_need_store		(	ex_data_need_store		),
	.ex_predict_correct		(	ex_predict_correct		),
	.ex_jalr_addr			(	ex_jalr_addr			),
	.ex_non_branch_addr		(	ex_non_branch_addr		),
	.ex_branch_taken		(	ex_branch_taken			)
);

ex_mem u_ex_mem
(
	.clk					(	clk						),
	.rst					(	rst						),
	.ex_mem_stall			(	stall[3]				),
	.ex_mem_flush			(	flush[3]				),
	.id_ex_rd_num			(	id_ex_rd_num			),	
	.id_ex_rd_we			(	id_ex_rd_we				),	
	.id_ex_rs1_num			(	id_ex_rs1_num			),
	.id_ex_rs1_re			(	id_ex_rs1_re			),
	.id_ex_rs2_num			(	id_ex_rs2_num			),
	.id_ex_rs2_re			(	id_ex_rs2_re			),	
	.ex_rd_value			(	ex_rd_value				),
	.id_ex_ls_type			(	id_ex_ls_type			),
	.id_ex_memory_re		(	id_ex_memory_re			),
	.id_ex_memory_we		(	id_ex_memory_we			),
	.ex_data_need_store		(	ex_data_need_store		),
	
	.ex_mem_rd_num			(	ex_mem_rd_num			),
	.ex_mem_rd_we			(	ex_mem_rd_we			),
	.ex_mem_rs1_num			(	ex_mem_rs1_num			),
	.ex_mem_rs1_re			(	ex_mem_rs1_re			),
	.ex_mem_rs2_num			(	ex_mem_rs2_num			),
	.ex_mem_rs2_re			(	ex_mem_rs2_re			),
	.ex_mem_rd_value		(	ex_mem_rd_value			),
	.ex_mem_ls_type			(	ex_mem_ls_type			),		
	.ex_mem_memory_re		(	ex_mem_memory_re		),		
	.ex_mem_memory_we		(	ex_mem_memory_we		),		
	.ex_mem_data_need_store	(	ex_mem_data_need_store	)
);

mem u_mem
(
	.clk					(	clk						),
	.rst					(	rst						),
	.ex_mem_ls_type			(	ex_mem_ls_type			),
	.ex_mem_memory_re		(	ex_mem_memory_re		),
	.ex_mem_memory_we		(	ex_mem_memory_we		),
	.ex_mem_rd_value		(	ex_mem_rd_value			),
	.ex_mem_data_need_store	(	ex_mem_data_need_store	),
	.mem_memory_dout		(	mem_memory_dout			),
	.mem_addr_align			(	mem_addr_align			),

	.ls_type				(	ls_type					),
	.re						(	re						),
	.we						(	we						),
	.addr					(	addr					),
	.din					(	din						),
	.dout					(	dout					),
	.addr_align				(	addr_align				)
);

unified_memory_model u_unified_memory_model
(
	.clk					(	clk						),
	.rst					(	rst						),
	.if_pc					(	pc						),
	.if_inst				(	if_inst					),
	.if_inst_align			(	if_inst_align			),
	.ls_type				(	ls_type					),
	.re						(	re						),
	.we						(	we						),
	.addr					(	addr					),
	.din					(	din						),
	.dout					(	dout					),
	.addr_align				(	addr_align				)
);

mem_wb u_mem_wb
(
	.clk					(	clk						),
	.rst					(	rst						),
	.mem_wb_stall			(	stall[4]				),
	.mem_wb_flush			(	flush[4]				),
	.ex_mem_rd_num			(	ex_mem_rd_num			),
	.ex_mem_rd_we			(	ex_mem_rd_we			),	
	.ex_mem_rd_value		(	ex_mem_rd_value			),
	.ex_mem_rs1_num			(	ex_mem_rs1_num			),
	.ex_mem_rs1_re			(	ex_mem_rs1_re			),
	.ex_mem_rs2_num			(	ex_mem_rs2_num			),
	.ex_mem_rs2_re			(	ex_mem_rs2_re			),	
	.ex_mem_memory_re		(	ex_mem_memory_re		),
	.mem_memory_dout		(	mem_memory_dout			),
	.mem_addr_align			(	mem_addr_align			),

	.mem_wb_rd_num			(	mem_wb_rd_num			),
	.mem_wb_rd_we			(	mem_wb_rd_we			),
	.mem_wb_rd_value		(	mem_wb_rd_value			),
	.mem_wb_memory_re		(	mem_wb_memory_re		),
	.mem_wb_memory_dout		(	mem_wb_memory_dout		),
	.mem_wb_addr_align		(	mem_wb_addr_align		),
	.mem_wb_rs1_num			(	mem_wb_rs1_num			),
	.mem_wb_rs1_re			(	mem_wb_rs1_re			),
	.mem_wb_rs2_num			(	mem_wb_rs2_num			),
	.mem_wb_rs2_re			(	mem_wb_rs2_re			)
	
);

wb u_wb
(
	.mem_wb_rd_num			(	mem_wb_rd_num			),
	.mem_wb_rd_we			(	mem_wb_rd_we			),
	.mem_wb_rd_value		(	mem_wb_rd_value			),
	.mem_wb_memory_re		(	mem_wb_memory_re		),
	.mem_wb_memory_dout		(	mem_wb_memory_dout		),
	.mem_wb_addr_align		(	mem_wb_addr_align		),
	.wb_rd_num				(	wb_rd_num				),
	.wb_rd_we				(	wb_rd_we				),
	.wb_rd_value			(	wb_rd_value				)
);



bpu u_bpu
(
	.clk					(	clk					),
	.rst					(	rst					),
	.id_jump_type			(	id_jump_type		),
	.id_branch_direction	(	id_branch_direction	),
	.id_jump_addr			(	id_jump_addr		),
	                        	                    
	.id_ex_jump_type		(	id_ex_jump_type		),
	.id_ex_jump_addr		(	id_ex_jump_addr		),
	.ex_non_branch_addr		(	ex_non_branch_addr	),
	.ex_branch_taken		(	ex_branch_taken		),
	                        	                    
	.ex_predict_correct		(	ex_predict_correct	),
	.ex_jalr_addr			(	ex_jalr_addr		),

	.bpu_id_flush			(	bpu_id_flush		),
	.bpu_ex_flush			(	bpu_ex_flush		),
	.bpu_flush_pc			(	bpu_flush_pc		)
);

ctrl u_ctrl
(
	.id_stall_for_lr_hazard	(	id_stall_for_lr_hazard	),
	.bpu_id_flush			(	bpu_id_flush			),
	.bpu_ex_flush			(	bpu_ex_flush			),
	
	.stall					(	stall					),
	.flush					(	flush					)
);


endmodule