//======================================================================
//  @Filename	:	ex.v
//  @Data		:	2021.04.11
//	@Author		:	Li Xiaocong
//	@Department	:	School of microelectronic
//	@Version	:	1.0
//  @Abstract	:	execution unit
//	@git		:	https://gitee.com/resfzdfdz/X5R.git
//
//	X5R is a basic 5 stage pipeline implementation of RISC-V ISA;
//	you can redistribute it and/or modify it under the terms of the GNU 
//	General Public License as published by the Free Software Foundation; 
//	either version 3, or (at your option) any later version.
//
//	X5R is distributed in the hope that it will be useful, but WITHOUT ANY
//	WARRANTY; without even the implied warranty of MERCHANTABILITY or
//	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
//	for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with X5R; see the file COPYING3.  If not see
//	<http://www.gnu.org/licenses/>.
//======================================================================
//	This module is execution unit, it include:
//		1. integer Unit
//		2. shift Unit
//		3. bypass network of functional unit
//		4. input selector of functional unit
//		5. output selector 
//		6. branch predictor verification
//======================================================================

module ex
#(
	parameter XLEN = 32
)
(
	input wire [XLEN-1:0]	id_ex_pc,
	input wire 				id_ex_align,
	//	For functional unit
	input wire [2:0] 		id_ex_int_unit_type_word,
	input wire [1:0] 		id_ex_int_unit_ctrl_word,
	input wire [1:0] 		id_ex_shift_unit_ctrl_word,
	//	Immediate number
	input wire [XLEN-1:0] 	id_ex_imm,
	//	Instruction valid
	input wire				id_ex_inst_valid,
	//	Bypass mux select
	input wire 				id_ex_mux_vj,
	input wire 				id_ex_mux_vk,
	input reg 				id_ex_mux_auipc,
	//	Output mux select
	input wire [2:0]		id_ex_mux_output,
	//	For memory control
	input wire [2:0]		id_ex_ls_type,
	input wire 				id_ex_memory_re,
	input wire 				id_ex_memory_we,
	//	For Reigster File
	input wire [4:0]		id_ex_rd_num,
	input wire 				id_ex_rd_we,
	input wire [4:0]		id_ex_rs1_num,
	input wire				id_ex_rs1_re,
	input wire [4:0]		id_ex_rs2_num,
	input wire				id_ex_rs2_re,
	//	Register File input
	input wire [XLEN-1:0]	id_ex_rs1_value,
	input wire [XLEN-1:0]	id_ex_rs2_value,
	
	//	Bypass Network
	input wire [4:0]		ex_mem_rd_num,
	input wire				ex_mem_rd_we,
	input wire [XLEN-1:0]	ex_mem_rd_value,
	
	input wire [4:0]		mem_wb_rd_num,
	input wire				mem_wb_rd_we,
	input wire [XLEN-1:0]	mem_wb_rd_value,
	
	input wire 				ex_mem_memory_re,
	input wire 				ex_mem_memory_we,
	//	For load bypass
	input wire [XLEN-1:0]	wb_rd_value,
	//	Branch Predictor
	input wire [1:0]		id_ex_jump_type,
	input wire				id_ex_branch_direction,
	input wire [XLEN-1:0]	id_ex_jump_addr,
	
	output wire [XLEN-1:0]	ex_rd_value,
	//	Memory Data
	output wire [XLEN-1:0]	ex_data_need_store,
	//	Branch Predictor Verification
	output wire 			ex_predict_correct,
	output wire [XLEN-1:0]	ex_jalr_addr,
	output wire [XLEN-1:0]	ex_non_branch_addr,
	output wire 			ex_branch_taken
);

//	Send data into functional unit
reg [XLEN-1:0] Vj, Vk;
wire [XLEN-1:0] reg_Vj, reg_Vk;

assign reg_Vj = Vj;
assign reg_Vk = Vk;

//	Instantiate Shift Unit
wire [1:0] 		shift_code;
wire [4:0] 		shift_num;
wire [XLEN-1:0] number;
wire [XLEN-1:0] shift_result;

assign shift_code = id_ex_shift_unit_ctrl_word;
assign shift_num  = Vk[4:0];
assign number	  = Vj;

ShiftUnit u_shift_unit
(
	.ShiftCode		(	shift_code		),
	.ShiftNum		(	shift_num		),
	.Number			(	number			),
	.ShiftUnitRes	(	shift_result	)
);

//	Instantiate Integer Unit
wire [XLEN-1:0] int_result;

IntUnit u_int_unit
(
	.Vj				(	reg_Vj			),
	.Vk				(	reg_Vk			),
	.IntUnitCode	(	id_ex_int_unit_type_word	),
	.ctrl			(	id_ex_int_unit_ctrl_word	),
	.IntUnitRes		(	int_result		)
);

//	Vj bypass network
always @(*)
begin
	if (id_ex_rs1_re && ex_mem_rd_we && (id_ex_rs1_num == ex_mem_rd_num) )
		Vj = ex_mem_rd_value;
	else if (id_ex_rs1_re && mem_wb_rd_we && (id_ex_rs1_num == mem_wb_rd_num) )
		Vj = wb_rd_value;		// bypass data may come from EX stage or memory;
	else if (id_ex_mux_auipc)
		Vj = id_ex_pc;
	else if (id_ex_mux_vj)
		Vj = id_ex_rs1_value;
	else
		Vj = 32'h0;
end

//	Vk bypass network
always @(*)
begin
	if (id_ex_memory_we)
		Vk = id_ex_imm;
	else if (id_ex_rs2_re && ex_mem_rd_we && (id_ex_rs2_num == ex_mem_rd_num) )
		Vk = ex_mem_rd_value;
	else if (id_ex_rs2_re && mem_wb_rd_we && (id_ex_rs2_num == mem_wb_rd_num) )
		Vk = wb_rd_value;		// bypass data may come from EX stage or memory;
	else if (id_ex_mux_vk)
		Vk = id_ex_rs2_value;
	else
		Vk = id_ex_imm;
end

//	Output select
reg [XLEN-1:0] result;

always @(*)
begin
	case (id_ex_mux_output)
		3'b001:		result = shift_result;
		3'b010:		result = int_result;
		3'b011:		result = id_ex_imm;			// For LUI
		3'b100:		result = id_ex_pc + 32'h4;	// For JALR 
		default:	result = 32'h0;
	endcase
end

assign ex_rd_value = result;

//	Get data need store from bypass network (not from id_ex_rs2_value)
reg [XLEN-1:0] data_need_store;

always @(*)
begin
	if (id_ex_rs2_re && ex_mem_rd_we && (id_ex_rs2_num == ex_mem_rd_num) )
		data_need_store = ex_mem_rd_value;
	else if (id_ex_rs2_re && mem_wb_rd_we && (id_ex_rs2_num == mem_wb_rd_num) )
		data_need_store = mem_wb_rd_value;
	else
		data_need_store = id_ex_rs2_value;
end

assign ex_data_need_store = data_need_store;

//	Branch Predictor Verification
assign ex_branch_taken = (int_result == 32'h1);
assign ex_predict_correct = (id_ex_jump_type == 2'b01) ? (ex_branch_taken ^~ id_ex_branch_direction) : 1'b1;
assign ex_jalr_addr	= int_result;
assign ex_non_branch_addr = id_ex_pc + 32'h4;

endmodule

